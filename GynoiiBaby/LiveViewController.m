//
//  LiveViewController.m
//  GynoiiBaby
//
//  Created by BingHuan Wu on 3/7/17.
//  Copyright © 2017 Gynoii. All rights reserved.
//

#import <Photos/Photos.h>

#import "LiveViewController.h"
#import "AddCameraViewController.h"
#import "ContentTemplateViewController.h"
#import "TwoWayAudioManager.h"

@interface LiveViewController () {
    // Compile as ObjC++
    TwoWayAudioManager *twoWayAudio;
}

@end

@implementation LiveViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    // Notification register
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cameraListUpdated) name:LIST_UPDATED_NOTIFICATION object:nil];
    
    // Setup UI once (color style and localization)
    [self setupUI];
    
    // Setup Child views (in scroll view)
    [self resetChildViews];
    
    // Two way audio setup
    twoWayAudio = [[TwoWayAudioManager alloc] init];
    
    // Pip view test
    pipView = [[SnapshotView alloc] init];
    [pipView setBackgroundColor:[UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:0.5]];
    [pipView setAlpha:0.9];
    [pipView setParentView:self.view];
//    [self.view addSubview:pipView];
}

- (void)resetChildViews {
    // Remove old stuff if there's any
    for (UIView *subview in [scrollView subviews]) {
        [subview removeFromSuperview];
    }
    for (UIViewController *vc in [self childViewControllers]) {
        [vc removeFromParentViewController];
    }
    
    // Scroll view setup
    NSMutableArray *cameraList = [CameraManager getCameraList];
    
    for (NSInteger i = 0; i < [cameraList count]; i++) {
        ContentTemplateViewController *camera = [[ContentTemplateViewController alloc] init];
        [camera setMode:[[[cameraList objectAtIndex:i] objectForKey:CAMERA_STATUS] boolValue]];
        [self addChildViewController:camera];
        [scrollView addSubview:camera.view];
    }
    
    // Add addCameraView in scrollview, also add as child view controller too
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AddCameraViewController *addCameraVC = [sb instantiateViewControllerWithIdentifier:@"addCameraID"];
    addCameraView = addCameraVC.view;
    [self addChildViewController:addCameraVC];
    [scrollView addSubview:addCameraView];
    
    // PageControl setup
    [pageControl setNumberOfPages:[cameraList count]+1];
    [pageControl setCurrentPage:0];
    
    // Initial title
    if ([cameraList count]) {
        NSDictionary *firstCam = [cameraList firstObject];
        NSString *title = [firstCam objectForKey:CAMERA_NAME];
        [customNavigationItem setTitle:title];
    }
    else {
        [customNavigationItem setTitle:NSLocalizedString(@"Add new camera", nil)];
    }
}

- (void)setupUI {
    // Programatic UI adjust
    [pageControl setCurrentPageIndicatorTintColor:GYNOII_GREEN];
    [pageControl setPageIndicatorTintColor:[UIColor colorWithRed:0.7 green:0.7 blue:0.7 alpha:1]];
    
    [[snapShotBtn layer] setBorderWidth:.5f];
    [[snapShotBtn layer] setBorderColor:[UIColor lightGrayColor].CGColor];
    
    [[talkBtn layer] setBorderWidth:.5f];
    [[talkBtn layer] setBorderColor:[UIColor lightGrayColor].CGColor];
    
    // Animation
    [recordBtn setImage:[UIImage animatedImageNamed:@"recording_animation_0" duration:1.5f] forState:UIControlStateSelected];
    [timelapseBtn setImage:[UIImage animatedImageNamed:@"timelapse_animation_0" duration:1.5f] forState:UIControlStateSelected];
    
    // TODO: Localization
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    // Remove notification observer
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    // Layout all child views
    [self updateScrollView];

    // Orientation handle
    [self handleLandScapeControl];
    
    // Menubar buttons update
    [self updateMenuBarButtons];
    
    // Try to play stream
    [self playCameraStream];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    // Clean up old player
    if (gynoiiPlayer) {
        [gynoiiPlayer pause];
        [gynoiiPlayer.view removeFromSuperview];
        [gynoiiPlayer removeFromParentViewController];
        gynoiiPlayer = nil;
//        NSLog(@"Clean up old player");
    }
}

- (void)updateScrollView {
    NSArray *childViewControllers = [self childViewControllers];
    NSUInteger count = [childViewControllers count];
    
    // Magic rect
    CGRect frame = [[UIScreen mainScreen] bounds];
    CGFloat oldWidth  = frame.size.width;
    CGFloat oldHeight = frame.size.height;
    
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    BOOL isLandscape = UIInterfaceOrientationIsLandscape(orientation);
    
    CGFloat newWidth = (isLandscape)? MAX(oldWidth, oldHeight): MIN(oldWidth, oldHeight);
    CGFloat newHeight = (isLandscape)? MIN(oldWidth, oldHeight): MAX(oldWidth, oldHeight);
    
    // Reset scroll view content size
    [scrollView setContentSize:CGSizeMake(newWidth*count, 10)];
    [scrollView setFrame:CGRectMake(0, 0, newWidth, newHeight)];
    
    // Recenter all child views
    for (NSInteger i = 0; i < count; i ++) {
        UIView *subview = ((UIViewController*)[childViewControllers objectAtIndex:i]).view;
        [subview setFrame:CGRectMake(newWidth*i, 0, newWidth, newHeight)];
    }
    
    // Pip view initial location
    if (isLandscape) {
        [pipView setFrame:CGRectMake(frame.size.width*2/3, frame.size.height-frame.size.width/4, frame.size.width/3, frame.size.width/4)];
    }
    else {
        [pipView setFrame:CGRectMake(frame.size.width/2, frame.size.height-184-frame.size.width*3/8, frame.size.width/2, frame.size.width*3/8)];
    }
    
    // Scroll to correct page
    [self changePage:nil];
}

- (void)updateMenuBarButtons {
    NSInteger index = pageControl.currentPage;
    NSMutableArray *cameraList = [CameraManager getCameraList];
    
    if (0 == [cameraList count] || index > [cameraList count] - 1) {
        return;
    }
    
    NSDictionary *cameraInfo = [cameraList objectAtIndex:index];
    BOOL lanValid = [[cameraInfo objectForKey:LAN_VALID] boolValue];
    BOOL satValid = [[cameraInfo objectForKey:SAT_VALID] boolValue] | lanValid;
    BOOL wanValid = [[cameraInfo objectForKey:WAN_VALID] boolValue] | lanValid;
    BOOL talkValid = [[cameraInfo objectForKey:TALK_VALID] boolValue] | lanValid;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        snapShotBtn.enabled = recordBtn.enabled = muteBtn.enabled = satValid;
        musicBtn.enabled = talkBtn.enabled = talkValid;
        ptzBtn.enabled = timelapseBtn.enabled = wanValid; // TODO: check ptz only for AONI
    });
}

- (ContentTemplateViewController*)getContentViewController {
    NSInteger index = pageControl.currentPage;
    NSArray *childViewControllers = [self childViewControllers];
    if (index < [childViewControllers count] - 1) {
        ContentTemplateViewController *vc = (ContentTemplateViewController*)[childViewControllers objectAtIndex:index];
        return vc;
    }
    return nil;
}

- (void)handleLandScapeControl {
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    BOOL isLandscape = UIInterfaceOrientationIsLandscape(orientation);
    [self setControlHidden:isLandscape];
    [encodeIndicatorTop setConstant:isLandscape? 10.0f: 64.0f];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    
    // Code here will execute before the rotation begins.
    // Equivalent to placing it in the deprecated method -[willRotateToInterfaceOrientation:duration:]
    
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        
        // Place code here to perform animations during the rotation.
        // You can pass nil or leave this block empty if not necessary.
        
        if (self.isViewLoaded && self.view.window) {
            [self handleLandScapeControl];
            [self updateScrollView];
        }
    } completion:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        
        // Code here will execute after the rotation has finished.
        // Equivalent to placing it in the deprecated method -[didRotateFromInterfaceOrientation:]

    }];
}

- (void)setControlHidden:(BOOL)hidden {
    dispatch_async(dispatch_get_main_queue(), ^{
        customNavBar.hidden = menuBar.hidden = pageControl.hidden = snapShotBtn.hidden = talkBtn.hidden = self.tabBarController.tabBar.hidden = hidden;
        [self.tabBarController.view setNeedsLayout];
    });
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



#pragma mark private
- (void)cameraListUpdated {
    // Menubar buttons update
    [self updateMenuBarButtons];
    
    // Try to play stream
    [self playCameraStream];
    
    // Try to update PIP
    [AppSetting updateAppSetting:PIP_INDEX withNewObject:@0]; // test
    [pipView startUpdate];
}

- (void)playCameraStream {
    NSInteger index = pageControl.currentPage;
//    NSLog(@"Try to play index %ld", (long)index);
    
    // Visibility check
    if (!(self.isViewLoaded && self.view.window)) {
        // View Controller is not visible
//        NSLog(@"View is not visible");
        return;
    }
    
    // Info validation
    NSMutableArray *cameraList = [CameraManager getCameraList];
    if (0 == [cameraList count] || index > [cameraList count] - 1) {
//        NSLog(@"no camera info or count mismatch, won't play");
        return;
    }
    
    NSMutableDictionary *camera = [cameraList objectAtIndex:index];
    
    
    
    // TODO : check more(LAN, audio reset, camera model) , test SAT for now
    if (![[camera objectForKey:SAT_VALID] boolValue] && ![[camera objectForKey:LAN_VALID] boolValue]) {
//        NSLog(@"no SAT nor LAN connection");
        return;
    }
    
    
    NSString *mediaURL = @"";
#ifdef FORCE_P2P
    if ([camera objectForKey:SAT_PATH]) {
        mediaURL = [NSString stringWithFormat:@"%@%@", [camera objectForKey:SAT_PATH], [camera objectForKey:STREAM_PATH_LOW]];
    }
    else {
        NSLog(@"No p2p path");
        return;
    }
#endif
    
    if (!gynoiiPlayer) {
        gynoiiPlayer = [[HomeMadeLiveViewController alloc] init];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (!gynoiiPlayer.playing) {
            ContentTemplateViewController *contentTemplate = [self getContentViewController];
            [contentTemplate addChildViewController:gynoiiPlayer];
            UIView *renderView = [contentTemplate getRenderView];
            [renderView addSubview:gynoiiPlayer.view];
            [gynoiiPlayer setDecoderPath:mediaURL];
            NSLog(@"opening %@",mediaURL);
        }
        else {
//            NSLog(@"already playing, won't restart");
        }
    });
}

- (void)promptPermissionWithMessage:(NSString*)message {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning", nil) message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleDestructive handler:nil];
    UIAlertAction *setting = [UIAlertAction actionWithTitle:NSLocalizedString(@"App Settings", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (UIApplicationOpenSettingsURLString) {
            NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
            [[UIApplication sharedApplication] openURL:url];
        }
        else {
            // Present some dialog telling the user to open the settings app.
            [Utility makeWarningToastWithTitle:NSLocalizedString(@"Warning", nil) andMessage:NSLocalizedString(@"Please authorize Gynoii Baby in Privacy Settings", nil)];
        }
    }];
    
    [alert addAction:cancel];
    [alert addAction:setting];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (PHAssetCollection *)checkIfCustomAlbumExist {
    NSString *gynoiiAlbum = ALBUM_TITLE;
    PHAssetCollection *gynoiiCollection = nil;
    
    // Check if custom album existed
    PHFetchResult *userCollection = [PHAssetCollection fetchTopLevelUserCollectionsWithOptions:nil];
    
    for (PHAssetCollection *collection in userCollection) {
        if ([collection localizedTitle]) {
            if ([gynoiiAlbum isEqualToString:[collection localizedTitle]]) {
                gynoiiCollection = collection;
                break;
            }
        }
    }
    
    return gynoiiCollection;
}

// Using PHPhotoLibrary - This function should be more cleaner
- (void)saveSnapshotToCustomAlbum {
    // Check permission first
    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
        if (PHAuthorizationStatusAuthorized != status) {
            [self promptPermissionWithMessage:NSLocalizedString(@"Gynoii Baby has no access to album", nil)];
        }
        else { // Album permission granted
            NSString *gynoiiAlbum = ALBUM_TITLE;
            PHAssetCollection *gynoiiCollection = [self checkIfCustomAlbumExist];
            
            // Create custom album and add photo
            if (!gynoiiCollection) {
                [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
                    [PHAssetCollectionChangeRequest creationRequestForAssetCollectionWithTitle:gynoiiAlbum];
                } completionHandler:^(BOOL success, NSError * _Nullable error) {
                    if (success) {
                        PHAssetCollection *gynoiiCollection = nil;
                        PHFetchResult *userCollection = [PHAssetCollection fetchTopLevelUserCollectionsWithOptions:nil];
                        for (PHAssetCollection *collection in userCollection) {
                            if ([collection localizedTitle]) {
                                if ([gynoiiAlbum isEqualToString:[collection localizedTitle]]) {
                                    gynoiiCollection = collection;
                                    break;
                                }
                            }
                        }
                        
                        // Get image
                        if (gynoiiPlayer) {
                            UIImage *snapshotImage = [gynoiiPlayer snapshotDidTouch:nil];
                            if (snapshotImage) {
                                [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
                                    PHAssetChangeRequest *createAssetRequest = [PHAssetChangeRequest creationRequestForAssetFromImage:snapshotImage];
                                    PHAssetCollectionChangeRequest *albumChangeRequest = [PHAssetCollectionChangeRequest changeRequestForAssetCollection:gynoiiCollection];
                                    PHObjectPlaceholder *placeHolder = [createAssetRequest placeholderForCreatedAsset];
                                    [albumChangeRequest addAssets:@[placeHolder]];
                                    
                                } completionHandler:^(BOOL success, NSError * _Nullable error) {
                                    if (error) {
                                        [Utility makeWarningToastWithTitle:NSLocalizedString(@"Error", nil) andMessage:[error localizedDescription]];
                                    }
                                    else {
                                        [Utility makeInfoToastWithTitle:NSLocalizedString(@"Hint", nil) andMessage:NSLocalizedString(@"Saved to gallery", nil)];
                                    }
                                }];
                            }
                            else {
                                // Wait
                                [Utility makeWarningToastWithTitle:NSLocalizedString(@"Warning", nil) andMessage:NSLocalizedString(@"Please wait a moment", nil)];
                            }
                        }
                    }
                    else if (error) { // Create album fail?
                        [Utility makeWarningToastWithTitle:NSLocalizedString(@"Error", nil) andMessage:[error localizedDescription]];
                    }
                }];
            }
            
            // Add to existed album directly
            else {
                // Get image
                if (gynoiiPlayer) {
                    UIImage *snapshotImage = [gynoiiPlayer snapshotDidTouch:nil];
                    if (snapshotImage) {
                        [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
                            PHAssetChangeRequest *createAssetRequest = [PHAssetChangeRequest creationRequestForAssetFromImage:snapshotImage];
                            PHAssetCollectionChangeRequest *albumChangeRequest = [PHAssetCollectionChangeRequest changeRequestForAssetCollection:gynoiiCollection];
                            PHObjectPlaceholder * placeHolder = [createAssetRequest placeholderForCreatedAsset];
                            [albumChangeRequest addAssets:@[placeHolder]];
                            
                        } completionHandler:^(BOOL success, NSError * _Nullable error) {
                            if (error) {
                                [Utility makeWarningToastWithTitle:NSLocalizedString(@"Error", nil) andMessage:[error localizedDescription]];
                            }
                            else {
                                [Utility makeInfoToastWithTitle:NSLocalizedString(@"Hint", nil) andMessage:NSLocalizedString(@"Saved to gallery", nil)];
                            }
                        }];
                    }
                    else {
                        // Wait
                        [Utility makeWarningToastWithTitle:NSLocalizedString(@"Warning", nil) andMessage:NSLocalizedString(@"Please wait a moment", nil)];
                    }
                }
            }
        }
    }];
}

- (void)createCustomAlbum {
    // Check permission first
    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
        if (PHAuthorizationStatusAuthorized != status) {
            [self promptPermissionWithMessage:NSLocalizedString(@"Gynoii Baby has no access to album", nil)];
        }
        else { // Album permission granted
            NSString *gynoiiAlbum = ALBUM_TITLE;
            PHAssetCollection *gynoiiCollection = [self checkIfCustomAlbumExist];
            
            // Create custom album if not found
            if (!gynoiiCollection) {
                [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
                    [PHAssetCollectionChangeRequest creationRequestForAssetCollectionWithTitle:gynoiiAlbum];
                } completionHandler:^(BOOL success, NSError * _Nullable error) {
                    if (error) { // Create album fail?
                        [Utility makeWarningToastWithTitle:NSLocalizedString(@"Error", nil) andMessage:[error localizedDescription]];
                    }
                }];
            }
        }
    }];
}

- (void)playMusicIndex:(int)musicIndex {
    if (nil == twoWayAudio) {
        NSLog(@"nil twoWayAudio (not likely)");
        return;
    }
    
    NSInteger index = pageControl.currentPage;
    NSMutableArray *cameraList = [CameraManager getCameraList];
    if (0 == [cameraList count] || index > [cameraList count] - 1) {
        return;
    }
    
    NSDictionary *cameraInfo = [cameraList objectAtIndex:index];
    BOOL lanValid = [[cameraInfo objectForKey:LAN_VALID] boolValue];
    int talkPort = [[cameraInfo objectForKey:TALK_PORT] intValue];
    
    if (!lanValid && 0 == talkPort) {
        return;
    }
    
#ifdef FORCE_P2P
    lanValid = NO;
    if (0 == talkPort) {
        NSLog(@"p2p not ready");
        return;
    }
#endif
    
    // Get param
    NSString *ip = lanValid? [cameraInfo objectForKey:LAN_IP]: LOCAL_HOST_IP;
    int port = lanValid? TALK_PORT_NUMBER: [[cameraInfo objectForKey:TALK_PORT] intValue];
    
    // Play music in background queue
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [twoWayAudio twoWayMusicWithIP:ip port:port musicIndex:musicIndex];
    });
    
    // UI update in main queue
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [musicBtn setSelected:([twoWayAudio getPlayState]? YES: NO)];
            NSLog(@"music is playing? %d", [twoWayAudio getPlayState]);
        });
    });
}

- (void)insertLocaMusicToList:(UIAlertController*)songList {
    // Transcoded music (in mulaw format)
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSArray *directoryContent = [fileManager contentsOfDirectoryAtPath:documentsDirectory error:NULL];
    
    int songIndex = 3;
    
    for (NSUInteger count = 0; count < [directoryContent count]; count++) {
        NSString *name = [directoryContent objectAtIndex:count];
        if([name hasSuffix:@"ulaw"]){
            UIAlertAction *action = [UIAlertAction actionWithTitle:[name stringByDeletingPathExtension] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self playMusicIndex:songIndex];
            }];
            
            [songList addAction:action];
            songIndex ++;
        }
    }
}

#pragma mark Menu functions
- (IBAction)snapshotDidTouch:(id)sender {
    // Snapshot button usage track
    [Utility logEvent:CLICK_SNAPSHOT];
    
    [self buttonUp:sender];
    [self saveSnapshotToCustomAlbum];
}

- (IBAction)talkDidTouch:(id)sender {
    // Talk button usage track
    [Utility logEvent:CLICK_TALK];
    
    // Check microphone permission with call back
    void (^callBack)(BOOL granted) = ^(BOOL granted) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (granted) {
                NSInteger index = pageControl.currentPage;
                NSMutableArray *cameraList = [CameraManager getCameraList];
                if (0 == [cameraList count] || index > [cameraList count] - 1) {
                    [self buttonUp:sender];
                    return;
                }
                
                NSDictionary *cameraInfo = [cameraList objectAtIndex:index];
                BOOL lanValid = [[cameraInfo objectForKey:LAN_VALID] boolValue];
                int talkPort = [[cameraInfo objectForKey:TALK_PORT] intValue];
                
                if (!lanValid && 0 == talkPort) {
                    [self buttonUp:sender];
                    return;
                }
                
#ifdef FORCE_P2P
                lanValid = NO;
                if (0 == talkPort) {
                    [self buttonUp:sender];
                    return;
                }
#endif
                
                // Get param
                NSString *ip = lanValid? [cameraInfo objectForKey:LAN_IP]: LOCAL_HOST_IP;
                int port = lanValid? TALK_PORT_NUMBER: [[cameraInfo objectForKey:TALK_PORT] intValue];
                
                // Try to send talk packet in background queue (self toggle)
                dispatch_async(dispatch_get_global_queue(0, 0), ^{
                    [twoWayAudio twoWayTalkWithIP:ip andPort:port];
                });
                
                // UI update
                [talkBtn setSelected:(twoWayAudio.isRecording? YES: NO)];
                [talkBtn setBackgroundColor:(twoWayAudio.isRecording? [UIColor orangeColor]:[UIColor whiteColor])];
            }
            else {
                [self promptPermissionWithMessage:NSLocalizedString(@"Gynoii Baby has no access to microphone", nil)];
                [self buttonUp:sender];
            }
        });
    };
    
    [Utility checkMicrophonePermission:callBack];
}

- (IBAction)musicDidTouch:(id)sender {
    // Music button usage track
    [Utility logEvent:CLICK_MUSIC];
    
    UIAlertController *songList = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Select music to play", nil) message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *lullaby1 = [UIAlertAction actionWithTitle:NSLocalizedString(@"Lullaby 1", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        // Play music with index 1
        [self playMusicIndex:1];
    }];
    UIAlertAction *lullaby2 = [UIAlertAction actionWithTitle:NSLocalizedString(@"Lullaby 2", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        // Play music with index 2
        [self playMusicIndex:2];
    }];
    
    UIAlertAction *customize = [UIAlertAction actionWithTitle:NSLocalizedString(@"Customize your music list", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        // Show music picker and transcode - song list permission?
        dispatch_async(dispatch_get_main_queue(), ^{
            [self performSegueWithIdentifier:@"customMusicSegue" sender:nil];
        });
    }];
    
    UIAlertAction *stop = [UIAlertAction actionWithTitle:NSLocalizedString(@"Stop", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        // TODO: Mutiple stop method
        [self playMusicIndex:0];
        
        // UI update
        dispatch_async(dispatch_get_main_queue(), ^{
            [musicBtn setSelected:NO];
        });
    }];
    
    
    [songList addAction:lullaby1];
    [songList addAction:lullaby2];
    
    // Insert transcoded local music
    // Factory method to return alert action with song title and play index start from 3
    [self insertLocaMusicToList:songList];
    
    [songList addAction:customize];
    [songList addAction:stop];
    
    [self presentViewController:songList animated:YES completion:nil];
}

- (IBAction)recordDidTouch:(id)sender {
    // Record button usage track
    [Utility logEvent:CLICK_RECORD];
    
    // Check permission and try crate custom album
    [self createCustomAlbum];
    
    // Self toggle recording
    BOOL ret = NO;
    BOOL auth = (PHAuthorizationStatusAuthorized == [PHPhotoLibrary authorizationStatus])? YES: NO;
    
    if (auth && gynoiiPlayer) {
        ret = [gynoiiPlayer recordDidTouch:sender];
    }
    
    [recordBtn setSelected:ret? YES: NO];
}

- (IBAction)timelapseDidTouch:(id)sender {
    // TimeLapse button usage track
    [Utility logEvent:CLICK_TIMELAPSE];
    
    // Check permission and try crate custom album
    [self createCustomAlbum];
    
    // Self toggle timelapse
    BOOL ret = NO;
    BOOL auth = (PHAuthorizationStatusAuthorized == [PHPhotoLibrary authorizationStatus])? YES: NO;
    
    if (auth && pipView) {
        ret = [pipView timelapseDidTouch:pageControl.currentPage];
    }
    else {
        // No album permission (what if create album fail?)
        return;
    }
    
    [timelapseBtn setSelected:ret? YES: NO];
}

- (IBAction)muteDidTouch:(id)sender {
    // Mute button usage track
    [Utility logEvent:CLICK_MUTE];
    
    BOOL ret = NO;
    if (gynoiiPlayer) {
        ret = [gynoiiPlayer muteDidTouch:sender];
    }
    
    [muteBtn setSelected:ret? NO: YES];
}

- (IBAction)ptzDidTouch:(id)sender {
    
}

- (IBAction)buttonDown:(id)sender {
    [sender setBackgroundColor:[UIColor orangeColor]];
}

- (IBAction)buttonUp:(id)sender {
    [sender setBackgroundColor:[UIColor whiteColor]];
}

#pragma mark UI Delegation
- (IBAction)changePage:(id)sender {
    CGRect frame = scrollView.frame;
    frame.origin.x = frame.size.width * pageControl.currentPage;
    frame.origin.y = 0;
    [scrollView scrollRectToVisible:frame animated:YES];
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)aScrollView {
    // Menubar buttons update
    [self updateMenuBarButtons];
}

-(void)scrollViewDidScroll:(UIScrollView *)aScrollView {
    // Calculate page number
    NSInteger pageNumber = roundf(aScrollView.contentOffset.x / (aScrollView.frame.size.width));
    
    // The moment to switch page indicator
    if (pageNumber != pageControl.currentPage) {
        // Update page control
        pageControl.currentPage = pageNumber;
        
        // Update camera title
        NSMutableArray *cameraList = [CameraManager getCameraList];
        if (pageNumber < [cameraList count]) {
            NSDictionary *cameraInfo = [cameraList objectAtIndex:pageNumber];
            NSString *title = [cameraInfo objectForKey:CAMERA_NAME];
            [customNavigationItem setTitle:title];
            
            // Clean up old player
            if (gynoiiPlayer) {
                [gynoiiPlayer pause];
                [gynoiiPlayer.view removeFromSuperview];
                [gynoiiPlayer removeFromParentViewController];
                gynoiiPlayer = nil;
//                NSLog(@"Clean up old player");
            }
            
            // Restart player
            [self playCameraStream];
        }
        else {
            [customNavigationItem setTitle:NSLocalizedString(@"Add new camera", nil)];
        }
    }
    
    // Always stop two way audio
    [twoWayAudio twoWayTalkWithIP:nil andPort:0];
}

@end
