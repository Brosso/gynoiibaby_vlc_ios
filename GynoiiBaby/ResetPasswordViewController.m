//
//  ResetPasswordViewController.m
//  GynoiiBaby
//
//  Created by BingHuan Wu on 3/15/17.
//  Copyright © 2017 Gynoii. All rights reserved.
//

#import "ResetPasswordViewController.h"

@interface ResetPasswordViewController ()

@end

@implementation ResetPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // Tap dismiss
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    
    // Setup UI
    [self setupUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dismissKeyboard {
    [nameOrEmail resignFirstResponder];
}

- (void)setupUI {
    // Custom spinner
    [spinner setTransform:CGAffineTransformMakeScale(1.5, 1.5)];
    [spinner setColor:GYNOII_GREEN];
    
    // TODO: localization
}

- (IBAction)resetPassword:(id)sender {
    [nameOrEmail resignFirstResponder];
    [spinner setHidden:NO];
    
    // Empty field
    if ([nameOrEmail.text isEqualToString:@""]) {
        [Utility makeWarningToastWithTitle:NSLocalizedString(@"Empty Email or Username", nil) andMessage:NSLocalizedString(@"Please enter a valid username", nil)];
        [spinner setHidden:YES];
        return;
    }
    
    void (^callBack)(NSURLResponse* response, NSData* data, NSError* connectionError) = ^(NSURLResponse* response, NSData* data, NSError* error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [spinner setHidden:YES];
        });
        
        if ([data length] >0 && error == nil) {
            NSError *err = nil;
            NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&err];
            
            if (!err) {
                NSString *status = [jsonDict objectForKey:@"status"];
                NSString *msg = [jsonDict objectForKey:@"error_msg"];
                if ([status isEqualToString:@"fail"]) {
                    [Utility makeWarningToastWithTitle:NSLocalizedString(@"Failure", nil) andMessage:msg];
                }
                else if ([status isEqualToString:@"success"]) {
                    [Utility makeInfoToastWithTitle:NSLocalizedString(@"Success", nil) andMessage:NSLocalizedString(@"Please check your email and follow instructions to reset your password", nil)];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.navigationController popViewControllerAnimated:YES];
                    });
                }
            }
        }
        else if ([data length] == 0 && error == nil) {
            [Utility makeWarningToastWithTitle:NSLocalizedString(@"Error", nil) andMessage:NSLocalizedString(@"Please try again later", nil)];
        }
        else if (error){
            [Utility makeWarningToastWithTitle:NSLocalizedString(@"Error", nil) andMessage:[error localizedDescription]];
        }
    };
    
    NSString *requestString = [NSString stringWithFormat:@"http://sat.qlync.com:80/backstage_user_register.php?command=password_recovery&name=%@",nameOrEmail.text];
    [Utility makeURLRequest:requestString withCallback:callBack];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark textfield delagate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

@end
