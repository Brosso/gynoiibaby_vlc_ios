//
//  SatManager.mm
//  SI201
//
//  Created by qlync on 12/11/2.
//  Copyright (c) 2012 Qlync Inc. All rights reserved.
//

#import "SatManager.h"
#import "Util.h"

@implementation SatManager

#pragma mark - Common

- (void)setDebugLevel:(NSInteger)level
{
    //------------------------------------------------------------------------------------------
    // void SAT_SDK_LIB_Debug::SetDebugLevel(DEBUG_LEVEL　debug_level);
    //-----------------------------------------------------------------
    // Define the minimum set of debugging output level.
    // debug_level is in a range of 1 to 10.
    //------------------------------------------------------------------------------------------
//    NSLog(@"%s Set SAT_SDK_LIB debug level to %ld.", __PRETTY_FUNCTION__, (long)level);
    SAT_SDK_LIB_Debug::SetDebugLevel((DEBUG_LEVEL)level);
}


#pragma mark - SAT

- (BOOL)initSat:(NSString*)username password:(NSString*)password
{
//    NSLog(@"%s Initial SAT with username: %@, password: %@", __PRETTY_FUNCTION__, username, password);
    if (username == nil || password == nil) {
        NSLog(@"%s Username and password cannot be empty.", __PRETTY_FUNCTION__);
        return NO;
    }
    
    // Load license.
    NSString *licenseFilename = @"license";
    NSString *licensePath =  [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:licenseFilename];
//    NSLog(@"%s Load license at path: %@", __PRETTY_FUNCTION__, licensePath);
	IP2PLicense *license = P2PFactory::CreateLicense([licensePath UTF8String]);
    
    // Request SAT Server and verify license file.
//    NSLog(@"%s Create SAT Request.", __PRETTY_FUNCTION__);
    p2pSatRequest = P2PFactory::CreateSATRequest([username UTF8String], [password UTF8String], license);
    if (!license->IsValid()) {
//        NSLog(@"%s Verify license failed.", __PRETTY_FUNCTION__);
        delete license;
        return NO;
    }
    p2pLicense = license;
    
    // Get signal server address/port from license.
//    NSLog(@"%s Get signal server address/port.", __PRETTY_FUNCTION__);
    p2pLicense->GetSignalServer(signal_server, &signal_server_port);
    
    return YES;
}

- (BOOL)destroySat
{
    NSLog(@"%s Destroy SAT.", __PRETTY_FUNCTION__);
    if (p2pSatRequest) {
        delete p2pSatRequest;
        p2pSatRequest = NULL;
    }
    
    if (p2pLicense) {
        delete p2pLicense;
        p2pLicense = NULL;
    }
    
    return YES;
}

- (void)getDeviceEntryList {
    const std::vector<DeviceEntry *> *device_entries;
    
    //------------------------------------------------------------------------------------------
    // int GetDeviceEntryList(const std::vector<DeviceEntry *>** device_entry_list, const char*　service_type=NULL, const char* device_type=NULL)
    //-----------------------------------------------------------------
    // Get SAT Device List　which devices belongs to the account (username/password)
    //------------------------------------------------------------------------------------------
    NSInteger ret = p2pSatRequest->GetDeviceEntryList(&device_entries);
    if (ret != SAT_SDK_LIB_RET_NULL_SUCCESS) {
        NSLog(@"%s Get device entry list fail.", __PRETTY_FUNCTION__);
        return;
    }
    
    int n_device_entries = (int)(device_entries->size());
//    NSLog(@"Number of device entries: %d", n_device_entries);
    for (int i = 0; i < n_device_entries; i++) {
        DeviceEntry *device_entry = (*device_entries)[i];
        std::cout << "--" << std::endl;
        std::cout << *device_entry << std::endl;
        //std::cout << (*device_entry).cloud_client_id << std::endl;
        //std::cout << (*device_entry).cloud_secret << std::endl;
        //std::cout << (*device_entry).cloud_refresh_id << std::endl;
    }
    return;
}

// Brosso custom version
- (NSMutableArray *)gynoii_getDeviceEntryList {
    
    const std::vector<DeviceEntry *> *device_entries;
    NSInteger ret = 0;
    @try {
        ret = p2pSatRequest->GetDeviceEntryList(&device_entries);
    }
    @catch (NSException *exception) {
        NSLog(@"p2pSatRequest->GetDeviceEntryList exception(%@) catch: %@",[exception name],[exception reason]);
        return nil;
    }
    
    if (ret != SAT_SDK_LIB_RET_NULL_SUCCESS) {
#ifdef DEBUG
        NSLog(@"%s Get device entry list fail.", __PRETTY_FUNCTION__);
#endif
        return nil;
    }
    
    int n_device_entries = ((int)device_entries->size());
    for (int i = 0; i < n_device_entries; i++) {
        DeviceEntry *device_entry = (*device_entries)[i];
//        std::cout << "--" << std::endl;
//        std::cout << *device_entry << std::endl;
        
        const char low[]  = {'R','V','L','O','\0'}; // default low quality string
        const char mid[]  = {'R','V','M','E','\0'}; // default mid quality string
        const char high[] = {'R','V','H','I','\0'}; // default high quality string
        const char snap[] = {'W','B','S','S','\0'}; // snapshot path
        
        if (device_entry->purpose == snap) {
            // C class
            string uidString = device_entry->uid;
            string pathString = device_entry->url_path;
            
            // ObjC class
            NSString *device_uid = [NSString stringWithUTF8String:uidString.c_str()];
            NSString *path = [NSString stringWithUTF8String:pathString.c_str()];
            
            [CameraManager updateCamera:device_uid withObject:path andKey:SNAPSHOT_PATH];
        }
        
        else if (device_entry->purpose == low) {
            // C class
            string uidString = device_entry->uid;
            string pathString = device_entry->url_path;
            string macString = device_entry->mac_address;
            string nameString = device_entry->device_name;
            NSUInteger port = device_entry->port;
            bool status = device_entry->is_signal_online;
            
            // ObjC class
            NSString *device_uid = [NSString stringWithUTF8String:uidString.c_str()];
            NSString *path = [NSString stringWithUTF8String:pathString.c_str()];
            NSString *mac = [NSString stringWithUTF8String:macString.c_str()];
            NSString *name = [NSString stringWithUTF8String:nameString.c_str()];
            NSNumber *portNumber = [NSNumber numberWithUnsignedInteger:port];
            NSNumber *statusNumber = [NSNumber numberWithBool:status];
            
            NSMutableDictionary *newCameraDict = [NSMutableDictionary dictionary];
            [newCameraDict setObject:device_uid forKey:CAMERA_UID];
            [newCameraDict setObject:path forKey:STREAM_PATH_LOW];
            [newCameraDict setObject:mac forKey:CAMERA_MAC];
            [newCameraDict setObject:name forKey:CAMERA_NAME];
            [newCameraDict setObject:portNumber forKey:SAT_PORT];
            [newCameraDict setObject:statusNumber forKey:CAMERA_STATUS];
            
//            NSLog(@"Brosso: batchUpdateCamera low");
            [CameraManager batchUpdateCamera:newCameraDict];
        }
        
        else if (device_entry->purpose == mid) {
            // C class
            string uidString = device_entry->uid;
            string pathString = device_entry->url_path;
            
            // ObjC class
            NSString *device_uid = [NSString stringWithUTF8String:uidString.c_str()];
            NSString *path = [NSString stringWithUTF8String:pathString.c_str()];
            
//            NSLog(@"Brosso: updateCamera mid");
            [CameraManager updateCamera:device_uid withObject:path andKey:STREAM_PATH_MID];
        }
        
        else if (device_entry->purpose == high ) {
            // C class
            string uidString = device_entry->uid;
            string pathString = device_entry->url_path;
            
            // ObjC class
            NSString *device_uid = [NSString stringWithUTF8String:uidString.c_str()];
            NSString *path = [NSString stringWithUTF8String:pathString.c_str()];
         
//            NSLog(@"Brosso: updateCamera high");
            [CameraManager updateCamera:device_uid withObject:path andKey:STREAM_PATH_HIGH];
        }
    }
    
    // Sync back to default
    [CameraManager syncCameraListToDefaults];
    
    return [CameraManager getCameraList];
}


#pragma mark - P2P

- (NSDictionary *)startCaller:(NSString *)targetUid port:(NSUInteger)targetPort
{
//    NSLog(@"%s", __PRETTY_FUNCTION__);
    NSInteger ret;
    
    // ==================
    // SAT Part
    // ==================
    //-------------------------------------------------------------------------------------------
    // int GetUid(char* uid, const char* mac_addr=NULL)
    //-------------------------------------
    // Set local UID.
    // uid must be char array and larger than 64 bytes.
    //-------------------------------------------------------------------------------------------
//    NSLog(@"%s Get caller ID.", __PRETTY_FUNCTION__);
    NSString *mac = [Util getMacAddress];
    if (mac == nil) {
        NSLog(@"%s Get mac address failed.", __PRETTY_FUNCTION__);
        return nil;
    }
    mac = [mac stringByReplacingOccurrencesOfString:@":" withString:@""]; 
    p2pSatRequest->GetUid(uid, [mac UTF8String]);
    
    
    // ==================
    // P2P Part
    // ==================
    // Create P2P Tunnel
//    NSLog(@"%s Create P2P tunnel.", __PRETTY_FUNCTION__);
	p2pTunnel = P2PFactory::CreateTunnelCaller();
    
    //------------------------------------------------------------------------------------------
    // int SetSignalServer(const char* address , unsigned short port)
    //-----------------------------------------------------------------
    // Set signal address and port which are got from License. This function should be called before Start().
    //------------------------------------------------------------------------------------------
//    NSLog(@"%s Set signal address and port.", __PRETTY_FUNCTION__);
    p2pTunnel->SetSignalServer(signal_server, signal_server_port);
    
    //------------------------------------------------------------------------------------------
    // int EnableConfigCache(const char* config_cache_filename, const unsigned short timeout_in_sec)
    //-----------------------------------------------------------------
    // Store some connecting information in a file cache to speed up the connection time before the cache is timed out.
    // It is not necessary to enable the cache for Callee.
    //------------------------------------------------------------------------------------------
    NSString* cacheFilename = @"config_cache.dat";
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *cacheFilePath = [documentsDirectory stringByAppendingPathComponent:cacheFilename];
//    NSLog(@"%s Enable config cache. %@", __PRETTY_FUNCTION__, cacheFilePath);
    p2pTunnel->EnableConfigCache([cacheFilePath UTF8String], 3600);
    
    //------------------------------------------------------------------------------------------
    // int Start()
    //--------------
    // Start a persistent connection to signal server. This connection will be used to exchange information with other peers.
    //------------------------------------------------------------------------------------------
//    NSLog(@"%s Start P2P.", __PRETTY_FUNCTION__);
	ret = p2pTunnel->Start();
    if (ret != SAT_SDK_LIB_RET_NULL_SUCCESS) {
        [self stopP2p];
        return nil;
    }
    
    //------------------------------------------------------------------------------------------
    // int Authenticate(const char* uid, const char* password)
    //---------------------------------------------------------------
    // Verify Activated Code (*password)
    // Authentication function will block calling and waiting for signal server until getting response to continued.
    // This function should be called after calling Start().
    //------------------------------------------------------------------------------------------
//    NSLog(@"%s Authenticate.", __PRETTY_FUNCTION__);
	ret = p2pTunnel->Authenticate(uid);
    if (ret != SAT_SDK_LIB_RET_NULL_SUCCESS) {
        [self stopP2p];
        return nil;
    }
    
    //------------------------------------------------------------------------------------------
    // int SetGoogleAuthentication( const char* client_id, const char* client_secret, const char* refresh_token )
    //---------------------------------------------------------------
    // Set google authentication to enable google relay.
    // This function should be called before calling Connect().
    //------------------------------------------------------------------------------------------
    if (cloudProvider) {
        if ([cloudProvider isEqualToString:@"google"]) {
            NSLog(@"%s Google authenticate.", __PRETTY_FUNCTION__);
            const char *client = [clientId UTF8String];
            const char *secret = [clientSecret UTF8String];
            const char *token = [refreshToken UTF8String];
            
            ret = p2pTunnel->SetGoogleAuthentication(client, secret, token);
            if (ret != SAT_SDK_LIB_RET_NULL_SUCCESS) {
                NSLog(@"%s Set google authentification failed.", __PRETTY_FUNCTION__);
            }
        }
        else {
            NSLog(@"%s Unsupported cloud provider.", __PRETTY_FUNCTION__);
        }
    }
    else {
//        NSLog(@"%s No cloud provider.", __PRETTY_FUNCTION__);
    }
    
    //-------------------------------------------------------------------------------------------
    // int Connect(const char* remote_id)
    //-------------------------------------
    // Connect to a peer with the ID specified in the remote_id argument.
    // The two peers will automatically negotiate how to send data to each other.
    // This function should be called after calling Authenticate().
    // Connect function will block calling and waiting for signal server until getting response to continued.
    //-------------------------------------------------------------------------------------------
//    NSLog(@"%s Connecting to callee %@", __PRETTY_FUNCTION__, targetUid);
    ret = p2pTunnel->Connect([targetUid UTF8String]);
    if (ret != SAT_SDK_LIB_RET_NULL_SUCCESS) {
//        NSLog(@"%s Connection to callee %@ failed (Brosso)", __PRETTY_FUNCTION__, targetUid);
        return nil;
    }
    
    // Connect tunnel to device <uid> on port.
//    NSLog(@"%s Connect tunnel to callee %@ on port %lu (Brosso)", __PRETTY_FUNCTION__, targetUid, (unsigned long)targetPort);
    unsigned short localPort;
    std::string localAddr = "127.0.0.1";
    
    NSInteger count = 20;
    while (YES) {
        //------------------------------------------------------------------------------------------------
        // int ConnectTunnel(const char* remote_id, unsigned char protocol, const char* target_addr, unsigned short target_port, unsigned short* p_local_port)
        //------------------------------------------------------------------------------------------------
        // Establish a TCP over UDP tunnel to a peer with the ID specified in the remote_id argument.
        // The target server address is specified in target_addr and target_port argument.
        // The local address and port for connection is returned in p_local_port and p_local_addr arguments.
        // Users have to call connect before calling this function, and users can establish multiple TCP over UDP tunnels on the same UDP connection.
        // However, the performance is not guaranteed.
        //------------------------------------------------------------------------------------------------
        ret = p2pTunnel->ConnectTunnel([targetUid UTF8String], IPPROTO_TCP, "127.0.0.1", targetPort, &localPort);
        if (ret != SAT_SDK_LIB_RET_NULL_TRY_AGAIN_LATER) {
            break;
        }
        
        // Timeout.
        if (count <= 0) {
            NSLog(@"%s Connect tunnel to callee %@ on port %lu timeout.", __PRETTY_FUNCTION__, targetUid, (unsigned long)targetPort);
            return nil;
        }
        sleep(1);
        count = count - 1;
    }
    
    if (ret != SAT_SDK_LIB_RET_NULL_SUCCESS) {
//        NSLog(@"%s Connect to callee %@ on port %lu failed. (Brosso)", __PRETTY_FUNCTION__, targetUid, (unsigned long)targetPort);
        return nil;
    }
    
    NSString *localAddrInString = [NSString stringWithCString:localAddr.c_str() encoding:NSUTF8StringEncoding];
    NSNumber *localPortInNumber = [NSNumber numberWithUnsignedShort:localPort];
    NSDictionary *tunnelInfo = [[NSDictionary alloc] initWithObjectsAndKeys:localAddrInString, @"address", localPortInNumber, @"port", nil];
    return tunnelInfo;
}

- (BOOL)stopP2p
{
//    NSLog(@"%s", __PRETTY_FUNCTION__);
    if (p2pTunnel != NULL) {
        //--------------------------------------------------------------------------------
        // P2PSignalClientEntryState GetState()
        //-------------
        // Get the current state of the link to the signal server.
        //---------------------------------------------------------------------------------
        if (p2pTunnel->GetState() != P2PSignalClientEntryState_Disconnected) {
            //--------------------------------------------------------------------------------
            // int Stop()
            //-------------
            // Stop the connection to signal server. After calling Stop(),
            // you will need to call Start() again to do NAT traversal and transmit data.
            //---------------------------------------------------------------------------------
            p2pTunnel->Stop();
        }
        
        if (NULL != p2pTunnel) {
            delete p2pTunnel;
        }
        
        p2pTunnel = NULL;
    }
    return YES;
}

- (NSInteger)getP2pStatus:(NSString *)targetUid {
    //------------------------------------------------------------------------------------------
    // TunnelLinkState GetTunnelLinkState(const char *client_id)
    //---------------------------------------------------------------
    // Get current connection status between the Callee/Caller.
    //------------------------------------------------------------------------------------------
    return p2pTunnel->GetTunnelLinkState([targetUid UTF8String]);
}

// get the connect mode of device uid
- (NSString*)getNegotiationResult:(NSString*)targetUid
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    if (targetUid == nil) {
        NSLog(@"%s Target UID cannot be null.", __PRETTY_FUNCTION__);
        return @"";
    }
    
    string type; // relay cloud
    string laddr;
    string raddr;
    
    if (p2pTunnel == nil) {
        return @"";
    }
    
    //------------------------------------------------------------------------------------------
    // int GetNegotiationResult(const std::string*, std::string*　p_laddr, std::string*　p_raddr)
    //---------------------------------------------------------------
    // Get P2P Connection Type and IPs
    //------------------------------------------------------------------------------------------
    p2pTunnel->GetNegotiationResult([targetUid UTF8String], &type, &laddr, &raddr);
    
    NSString *connectionType = [NSString stringWithCString:type.c_str() encoding:[NSString defaultCStringEncoding]];
    NSLog(@"%s P2P connection type: %@", __PRETTY_FUNCTION__, connectionType);
    return connectionType;
}


#pragma mark - Cloud

- (void)connectGoogleService:(NSString *)cid secret:(NSString *)secret token:(NSString *)token
{
    cloudProvider = @"google";
    clientId = cid;
    clientSecret = secret;
    refreshToken = token;
    
    googleOAuth2 = new GoogleOAuth2([clientId UTF8String], [clientSecret UTF8String], [refreshToken UTF8String]);
    googleOAuth2->GetAccessToken();
    googleDocListDownloader = new GoogleDocListDownloader(googleOAuth2);
}

- (void)getGoogleInfo
{
    if (googleOAuth2 == nil || googleDocListDownloader == nil) {
        return;
    }
    
    // Get email.
    std::string email = googleOAuth2->GetUserEmail();
    
    // Get quota.
    pj_uint64_t total = 0, used = 0;
    googleDocListDownloader->GetQuota(&total, &used, NULL);
    
    NSLog(@"%s Email: %s, Quota: %llu/%llu", __PRETTY_FUNCTION__, email.c_str(), used, total);
}

- (void)getDeviceBackupFromGoogle:(NSString *)targetUid filename:(NSString *)filename
{
    if (googleDocListDownloader == nil) {
        return;
    }
    
    std::vector<GoogleDocListEntry *> entry_list;
    GoogleDocListEntry root_list;
    
    // Get google documents.
    NSLog(@"%s Start to get google entry list.", __PRETTY_FUNCTION__);
    
    // - Get 'root' entry
    if (googleDocListDownloader->GetEntryList(entry_list) != 0) {
        NSLog(@"%s Get google 'root' entry list failed.", __PRETTY_FUNCTION__);
        return;
    }
    
    BOOL found = NO;
    for (int i = 0; i < entry_list.size(); i++) {
        // Find Recordings directory.
        if (entry_list[i]->m_title == "Recordings") {
            root_list = *entry_list[i];
            found = YES;
            break;
        }
    }
    if (found == NO) {
        NSLog(@"%s Google 'recordings' entry not found.", __PRETTY_FUNCTION__);
        return;
    }
    
    // - Get 'Recordings' entry
    if (googleDocListDownloader->GetEntryList(entry_list, &root_list) != 0) {
        NSLog(@"%s Get google 'recordings' entry list failed.", __PRETTY_FUNCTION__);
        return;
    }
    
    // Compute device UID.
    string targetUidStr([targetUid UTF8String]);
    found = NO;
    for (int i = 0; i < entry_list.size(); i++) {
        // Find the UID directory.
        if (entry_list[i]->m_title.compare(targetUidStr) == 0) {
            root_list = *entry_list[i];
            found = YES;
            break;
        }
    }
    if (found == NO) {
        NSLog(@"%s Google %@ entry not found.", __PRETTY_FUNCTION__, targetUid);
        return;
    }
    
    // - Iterative get 'uid' entry, 100 records a time.
    googleDocListDownloader->GetEntryList(entry_list, &root_list, true, true, NULL, false, 100);
    int ret = 0;
    while (ret == 0) {
        ret = googleDocListDownloader->GetEntryListNext(entry_list);
    }
    
    // If filename exists, we could download the file from google drive.
    BOOL needDownload = (filename == nil || filename.length == 0) ? NO : YES;
    if (needDownload) {
        // Download file path.
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *filePath = [documentsDirectory stringByAppendingPathComponent:filename];
        string filePathStr([filePath UTF8String]);
        
        for (int i = 0; i < entry_list.size(); i++) {
            // Find the file.
            if (entry_list[i]->m_title.compare(filePathStr) == 0) {
                // Downloadthe file.
                if (googleDocListDownloader->DownloadFile(entry_list[i], [filePath UTF8String]) == 0) {
                    NSLog(@"%s Download file success.", __PRETTY_FUNCTION__);
                }
                else {
                    NSLog(@"%s Download file failed.", __PRETTY_FUNCTION__);
                }
                break;
            }
        }
    }
    else {
        // Iterator files.
//        for (int i = 0; i < entry_list.size(); i++) {
//            GoogleDocListEntry* entry = entry_list[i];
//        }
    }
    
    return;
}

@end
