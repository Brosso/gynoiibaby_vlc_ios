//
//  HomeMadeLiveViewController.h
//  Babycam
//
//  Created by Brosso on 7/30/14.
//  Copyright (c) 2014 Gynoii. All rights reserved.
//

#import <UIKit/UIKit.h>

@class KxMovieDecoder;

extern NSString * const KxMovieParameterMinBufferedDuration;    // Float
extern NSString * const KxMovieParameterMaxBufferedDuration;    // Float
extern NSString * const KxMovieParameterDisableDeinterlacing;   // BOOL

@interface HomeMadeLiveViewController : UIViewController <UIScrollViewDelegate>

- (UIImage *) snapshotDidTouch: (id) sender;
- (BOOL) recordDidTouch: (id) sender;
- (BOOL) muteDidTouch:(id) sender;
- (BOOL) isRecordingStart;
- (BOOL) isMute;

- (void)setDecoderPath:(NSString*)path;

@property (readonly) BOOL playing;

- (void) play;
- (void) pause;

@end
