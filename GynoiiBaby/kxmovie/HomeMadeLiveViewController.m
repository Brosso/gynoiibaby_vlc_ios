//
//  HomeMadeLiveViewController.m
//  Babycam
//
//  Created by Brosso on 7/30/14.
//  Copyright (c) 2014 Gynoii. All rights reserved.
//

#import <MediaPlayer/MediaPlayer.h>
#import <QuartzCore/QuartzCore.h>
#import <AVFoundation/AVFoundation.h>
#import "KxMovieDecoder.h"
#import "KxAudioManager.h"
#import "KxMovieGLView.h"
#import "KxLogger.h"
#import "HomeMadeLiveViewController.h"

NSString * const KxMovieParameterMinBufferedDuration = @"KxMovieParameterMinBufferedDuration";
NSString * const KxMovieParameterMaxBufferedDuration = @"KxMovieParameterMaxBufferedDuration";
NSString * const KxMovieParameterDisableDeinterlacing = @"KxMovieParameterDisableDeinterlacing";

#define LOCAL_MIN_BUFFERED_DURATION   0.2
#define LOCAL_MAX_BUFFERED_DURATION   0.4
#define NETWORK_MIN_BUFFERED_DURATION 0.0
#define NETWORK_MAX_BUFFERED_DURATION 4.0

#define MAX_ZOOM_SCALE 4.0
#define RECORD_FRAME_RATE 10.0

@interface HomeMadeLiveViewController () {
    KxMovieDecoder      *_decoder;
    dispatch_queue_t    _dispatchQueue;
    NSMutableArray      *_videoFrames;
    NSMutableArray      *_audioFrames;
    NSMutableArray      *_subtitles;
    NSData              *_currentAudioFrame;
    NSUInteger          _currentAudioFramePos;
    CGFloat             _moviePosition;
    
    NSTimeInterval      _tickCorrectionTime;
    NSTimeInterval      _tickCorrectionPosition;

    KxVideoFrame        *_currentPresentVideoFrame;
    
    KxMovieGLView       *_glView;
    UIImageView         *_imageView;
    UIScrollView        *_scrollView;
    
    UITapGestureRecognizer *_tapGestureRecognizer;
    UITapGestureRecognizer *_doubleTapGestureRecognizer;
    UIPanGestureRecognizer *_panGestureRecognizer;
    
    CGFloat             _bufferedDuration;
    CGFloat             _minBufferedDuration;
    CGFloat             _maxBufferedDuration;
    BOOL                _buffered;

    NSDictionary        *_parameters;

    BOOL                _recording;
    BOOL                _backgroundMode;
    BOOL                _recoverFromBackground;
    
    // Background audio refresh
    NSTimer             *_audioRefreshTimer;
}

@property (readwrite, atomic) BOOL playing;
@property (readwrite) BOOL busying;
@property (readwrite) BOOL decoding;
@property (readwrite, strong) KxArtworkFrame *artworkFrame;

@end

@implementation HomeMadeLiveViewController

- (void)setDecoderPath:(NSString*)path {
    
    if (self.busying) {
//        NSLog(@"qynoii player busy");
        return;
    }
    self.busying = YES;
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    
    // disable deinterlacing for iPhone, because it's complex operation can cause stuttering
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        parameters[KxMovieParameterDisableDeinterlacing] = @(YES);
        
        // disable buffering
        parameters[KxMovieParameterMinBufferedDuration] = @(0.0f);
        parameters[KxMovieParameterMaxBufferedDuration] = @(2.0f);
    }
    
    _moviePosition = 0;
    _parameters = parameters;
    
    __weak HomeMadeLiveViewController *weakSelf = self;
    KxMovieDecoder *decoder = [[KxMovieDecoder alloc] init];
    
    decoder.interruptCallback = ^BOOL(){
        __strong HomeMadeLiveViewController *strongSelf = weakSelf;
        return strongSelf ? NO : YES;
    };
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        NSError *error = nil;
        [decoder openFile:path error:&error];

        __strong HomeMadeLiveViewController *strongSelf = weakSelf;
        if (strongSelf) {
            dispatch_sync(dispatch_get_main_queue(), ^{
                [strongSelf setMovieDecoder:decoder withError:error];
            });
        }
    });
}

//- (void) loadContentWithIndex: (int)index {
//    if (index == _pageIndex && !_backgroundMode) {
//        return;
//    }
//    
//    if (_backgroundMode) {
//        NSLog(@"Trying to recover from background mode");
//        _recoverFromBackground = YES;
//    }
//    
//    self.view.hidden = YES;
//    
//    // TODO: refine LAN/TUNNEL priority and handle OFFLINE
//    
//    _pageIndex = index;
//    NSArray *deviceList = [AppDefault getAppSettingWithKey:DEVICE_LIST];
//    NSDictionary *dict = [deviceList objectAtIndex:index];
//    
//    BOOL status = [[dict objectForKey:@"STATUS"] boolValue];
//    NSString *lanIP = [dict objectForKey:@"LAN_IP"];
//    BOOL lanValid = [[dict objectForKey:@"LAN_VALID"] boolValue];
//    BOOL satValid = [[dict objectForKey:@"SAT_VALID"] boolValue];
//
//    NSString *UID = [dict objectForKey:@"UID"];
//    CGIMapper *mapper = [CGIMapper sharedInstance];
//    NSString *vender = [mapper checkVender:UID];
//    NSString *credential = [vender isEqualToString:@"JOVISION"]?@"admin:d1401667a9200ce3107@":@"";
//    
//    if (!status && !lanValid) {
//        return;
//    }
//    
//    NSString *servicePath = [dict objectForKey:@"PATH"];
//    NSString *path = @"";
//    NSString *resetAudioPath = @"";
//    // http://192.168.1.104/cgi-bin/jvsweb.cgi?cmd=webaudio&action=set&param="encType":2}
//    // http://192.168.1.104/cgi-bin/jvsweb.cgi?cmd=webaudio&action=set&param=%%7B%%22encType%%22%%3A2%%7D
//    
//    
//    if (YES == lanValid) { 
//        path = [NSString stringWithFormat:@"rtsp://%@%@%@",credential,lanIP,servicePath];
//        resetAudioPath = [NSString stringWithFormat:@"http://%@/cgi-bin/jvsweb.cgi?cmd=webaudio&action=set&param=%%7B%%22encType%%22%%3A2%%7D",lanIP];
//        _lanMode = YES;
//    }
//    else if (YES == satValid) {
//        path = [NSString stringWithFormat:@"%@%@",[dict objectForKey:@"SAT_PATH"],servicePath];
//        resetAudioPath = [NSString stringWithFormat:@"http://127.0.0.1:%d/cgi-bin/jvsweb.cgi?cmd=webaudio&action=set&param=%%7B%%22encType%%22%%3A2%%7D",[[dict objectForKey:@"SAT_WAN_PORT"] intValue]];
//        _lanMode = NO;
//    }
//    
//    if (![path isEqualToString:@""])
//    {
//#ifdef DEBUG
//        NSLog(@"opening path: %@",path);
//#endif
//        if ([path isEqualToString:_decoder.path]) {
//            return;
//        }
//        if (SHOWMODE) {
//            [Utility showAlertWithTitle:@"Message" andMessage:_lanMode?[NSString stringWithFormat:@"LAN mode: %@",lanIP]:[NSString stringWithFormat:@"SAT mode: %@\n WAN service: %d",[dict objectForKey:@"SAT_PATH"], [[dict objectForKey:@"SAT_WAN_PORT"] intValue]]];
//        }
//
//        if ([vender isEqualToString:@"JOVISION"] && ![resetAudioPath isEqualToString:@""]) {
////            NSLog(@"resetting jovision audio:%@",resetAudioPath);
//            [Utility makeURLRequest:resetAudioPath];
//        }
//        
//        // log local or remote mode
//        [Utility logEvent:_lanMode?VIEW_LOCAL:VIEW_REMOTE];
//        
//        [self setDecoderPath:path];
//    }
//    else
//    {
//        // blank path and yet online? -> how come?
//#ifdef DEBUG
//        NSLog(@"blank URL path on %@",dict);
//#endif
//        //[[NSNotificationCenter defaultCenter] postNotificationName:STREAM_ERR object:nil];
//    }
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(restoreZoom)];
    // Resize live view zoom scale
    tap.numberOfTapsRequired = 2;
    [self.view addGestureRecognizer:tap];
    
    // Var init
    _recording = NO;
    _backgroundMode = NO;
    _recoverFromBackground = NO;
    
    // Audio init
    id<KxAudioManager> audioManager = [KxAudioManager audioManager];
    [audioManager activateAudioSession];

}

- (void)dealloc {
    [self pause];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    id<KxAudioManager> audioManager = [KxAudioManager audioManager];
    [audioManager deactivateAudioSession];
    
    if (_dispatchQueue) {
        _dispatchQueue = NULL;
    }

    LoggerStream(1, @"%@ dealloc", self);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    if (self.playing) {
        
        [self pause];
        [self freeBufferedFrames];
        
        if (_maxBufferedDuration > 0) {
            
            _minBufferedDuration = _maxBufferedDuration = 0;
            [self play];
            
            LoggerStream(0, @"didReceiveMemoryWarning, disable buffering and continue playing");
            
        } else {
            // force ffmpeg to free allocated memory
            [_decoder closeFile];
//            [[NSNotificationCenter defaultCenter] postNotificationName:STREAM_EOF object:nil];
            [self backgroundServiceNotify];
        }
        
    } else {
        [self freeBufferedFrames];
        [_decoder closeFile];
//        [[NSNotificationCenter defaultCenter] postNotificationName:STREAM_EOF object:nil];
    }
}

- (void)viewDidAppear:(BOOL)animated {
//    LoggerStream(1, @"Homemade Live view viewDidAppear %@", self);
    [super viewDidAppear:animated];
    
    if (_decoder) {
        [self play];
    }
    
    // Notification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(frameViewHack) name:UIDeviceOrientationDidChangeNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationWillResignActive:)
                                                 name:UIApplicationWillResignActiveNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationDidBecomeActive:)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
//    LoggerStream(1, @"Homemade Live view viewWillDisappear %@", self);
    [super viewWillDisappear:animated];
    
    if (_decoder) {
        [self pause];
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
    
    _buffered = NO;
}

- (void)applicationWillResignActive: (NSNotification *)notification {
//    if (![[AppDefault getAppSettingWithKey:AUDIO_MODE] boolValue] && ![[AppDefault getAppSettingWithKey:TIMELAPSE_MODE] boolValue]) {
//        [self pause];
//    }
//    else if ([[AppDefault getAppSettingWithKey:AUDIO_MODE] boolValue])
//    {
//        UILocalNotification *localNotification = [[UILocalNotification alloc] init];
//        
//        //localNotification.fireDate = [NSDate date];
//        localNotification.alertBody = NSLocalizedString(@"Entered Audio mode", nil);
//        localNotification.soundName = UILocalNotificationDefaultSoundName;
//        [[UIApplication sharedApplication] cancelLocalNotification:localNotification];
//        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
//    }
//    else if ([[AppDefault getAppSettingWithKey:TIMELAPSE_MODE] boolValue])
//    {
//        id<KxAudioManager> audioManager = [KxAudioManager audioManager];
//        [audioManager setCatagory:NO];
//    }

    _backgroundMode = YES;
    LoggerStream(1, @"applicationWillResignActive");
    
    // Audio refresh in background mode (every 3 mins)
//    if ([[AppDefault getAppSettingWithKey:AUDIO_MODE] boolValue]) {
//        _audioRefreshTimer = [NSTimer scheduledTimerWithTimeInterval:90.0 target:self selector:@selector(backgroundServiceNotify) userInfo:nil repeats:YES];
//    }
}

- (void)applicationDidBecomeActive: (NSNotification *)notification {
//    if (![[AppDefault getAppSettingWithKey:AUDIO_MODE] boolValue] && ![[AppDefault getAppSettingWithKey:TIMELAPSE_MODE] boolValue]) {
//        if (_decoder.path) {
//            LoggerStream(1, @"restore playing");
//            [self play];
//        }
//    }
//    
//    else if ([[AppDefault getAppSettingWithKey:TIMELAPSE_MODE] boolValue]) {
//        id<KxAudioManager> audioManager = [KxAudioManager audioManager];
//        [audioManager play];
//    }
    
    _backgroundMode = NO;
    LoggerStream(1, @"applicationDidBecomeActive");
    
    // Stop audio refresh timer
    if ([_audioRefreshTimer isValid]) {
        [_audioRefreshTimer invalidate];
        _audioRefreshTimer = nil;
    }
}

#pragma mark - public

-(void)play {
    if (self.playing)
        return;
    
    
    self.playing = YES;
    _tickCorrectionTime = 0;

    

    [self asyncDecodeFrames];
    
    
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.1 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self tick];
    });
    
    if (_decoder.validAudio) {
        [self enableAudio:YES];
        
//        if ([[AppDefault getAppSettingWithKey:USER_GUEST_LOGIN] boolValue]) {
//            NSNumber *guestAudio = [AppDefault getAppSettingWithKey:USER_GUEST_AUDIO];
//            if (![guestAudio boolValue]) {
//                [self enableAudio:NO];
//                [Utility makeInfoToastWithTitle:NSLocalizedString(@"Hint", nil) andMessage:@"Audio is muted by host"];
//            }
//        }
    }
    
    LoggerStream(1, @"play movie");
//    [[NSNotificationCenter defaultCenter] postNotificationName:STREAM_PLAY object:nil];

}

- (void)pause {
    if (!self.playing)
        return;
    
    self.playing = NO;
    [self enableAudio:NO];
    LoggerStream(1, @"pause movie");
    [self freeBufferedFrames];
}

#pragma mark - actions
- (UIImage *)snapshotDidTouch:(id)sender {
    UIImage *currentImage = nil;
    
    if ([_currentPresentVideoFrame format] == KxVideoFrameFormatYUV) {
        //currentImage = [_glView glToUIImage]; // dump from OpenGL, buggy?
        currentImage = [_decoder decodeAsUIImage]; // decoder != renderer
    }
    else if ([_currentPresentVideoFrame format] == KxVideoFrameFormatRGB) {
        currentImage = [(KxVideoFrameRGB *)_currentPresentVideoFrame asImage];
    }
    
    if (nil == currentImage) {
        [Utility makeWarningToastWithTitle:NSLocalizedString(@"Snapshot Error", nil) andMessage:NSLocalizedString(@"Snapshot Error", nil)];
    }
    
    return currentImage;
}

- (void)snapshotDidTouch2: (id) sender {
    static int index = 0;
    UIImage *currentImage = nil;
    
    if ([_currentPresentVideoFrame format] == KxVideoFrameFormatYUV) {
        //currentImage = [_glView glToUIImage]; // dump from OpenGL
        currentImage = [_decoder decodeAsUIImage]; // decoder != renderer
    }
    else if ([_currentPresentVideoFrame format] == KxVideoFrameFormatRGB) {
        currentImage = [(KxVideoFrameRGB *)_currentPresentVideoFrame asImage];
    }
    
    if (nil == currentImage) {
        [Utility makeWarningToastWithTitle:NSLocalizedString(@"Snapshot Error", nil) andMessage:NSLocalizedString(@"Snapshot Error", nil)];
        return;
    }
    
    if (!sender) { // Normal snapshot
        //UIImageWriteToSavedPhotosAlbum(currentImage, nil, nil, nil);

        //watermark test
//        UILabel *newLabel = [[UILabel alloc] initWithFrame:CGRectZero];
//        newLabel.backgroundColor = [UIColor clearColor];
//        newLabel.font = [UIFont boldSystemFontOfSize:20.0];
//        newLabel.textAlignment = NSTextAlignmentCenter;
//        newLabel.textColor = [UIColor whiteColor];
//        newLabel.text = @"安安你好";
//        newLabel.backgroundColor = [UIColor clearColor];
//        [newLabel sizeToFit];
//        
//        UIImage *watermark = [UIImage imageNamed:@"snapshot_active.png"];
//        UIGraphicsBeginImageContextWithOptions(currentImage.size, FALSE, 1.0);
//        [currentImage drawInRect:CGRectMake(0, 0, currentImage.size.width, currentImage.size.height)];
//        [watermark drawInRect:CGRectMake(20, 50, watermark.size.width, watermark.size.height)];
//        [newLabel drawTextInRect:CGRectMake(20, 100, newLabel.frame.size.width, newLabel.frame.size.height)];
//        UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
//        UIGraphicsEndImageContext();
//        currentImage = newImage;
        
        /*
        ALAssetsLibrary *lib = [Utility defaultAssetsLibrary];//[ALAssetsLibrary new];
        [lib saveImage:currentImage toAlbum:NSLocalizedString(@"Baby Cam", nil) withCompletionBlock:^(NSError *error) {
            if (error != nil || nil == currentImage) {
                //[Utility showAlertWithTitle:NSLocalizedString(@"Snapshot Error", nil) andMessage:NSLocalizedString(@"Snapshot Error", nil)];
                [Utility makeWarningToastWithTitle:NSLocalizedString(@"Snapshot Error", nil) andMessage:NSLocalizedString(@"Snapshot Error", nil)];
            }
            else {
                //[Utility showAlertWithTitle:NSLocalizedString(@"Snapshot Done", nil) andMessage:NSLocalizedString(@"Image Saved to iPhone Gallery", nil)];
                [Utility makeInfoToastWithTitle:NSLocalizedString(@"Snapshot Done", nil) andMessage:NSLocalizedString(@"Image Saved to iPhone Gallery", nil)];
            }
        }];
        */
    }
    else { // Timelapse
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        
        BOOL isDir = NO;
        NSString *tmp = [documentsDirectory stringByAppendingPathComponent:@"TimeLapse"];
        if (![[NSFileManager defaultManager] fileExistsAtPath:tmp isDirectory:&isDir]) {
//            NSLog(@"Create folder for timelapse video frames");
            [[NSFileManager defaultManager] createDirectoryAtPath:tmp withIntermediateDirectories:NO attributes:nil error:nil];
            index = 0;
        }
        
        NSString *savedFileName = [NSString stringWithFormat:@"TimeLapse/%05d.png",index];
        NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:savedFileName];
        NSData *imageData = UIImagePNGRepresentation(currentImage);
        //NSData *imageData = UIImageJPEGRepresentation(currentImage, 1.0);
        if ([imageData writeToFile:savedImagePath atomically:NO]) {
            //NSLog(@"file saved: %@", savedImagePath);
        }
        
        index += 1;
    }
}

- (BOOL)isRecordingStart {
    return _recording;
}

- (BOOL)recordDidTouch:(id)sender {
    if (!_recording) {
        _recording = YES;
        [_decoder setupRecorder];
    }
    else {
        _recording = NO;
        [_decoder endRecorder];
    }
    return _recording;
}

- (BOOL)muteDidTouch:(id)sender {
    id<KxAudioManager> audioManager = [KxAudioManager audioManager];
    if ([audioManager playing]) {
        [audioManager pause];
        //NSLog(@"audio mute");
    }
    else {
        [audioManager play];
        //NSLog(@"audio resume");
    }
    return [audioManager playing];
}

- (BOOL) isMute {
    id<KxAudioManager> audioManager = [KxAudioManager audioManager];
    return [audioManager playing];
}


#pragma mark - private

- (void) setMovieDecoder: (KxMovieDecoder *) decoder
               withError: (NSError *) error {
    LoggerStream(2, @"setMovieDecoder");
    
    if (!error && decoder) {
        
        // Dealloc old decoder?
        if (_decoder && self.playing) {
            [_decoder closeFile];
            _decoder = nil;
        }
        
        _decoder        = decoder;
        _dispatchQueue  = dispatch_queue_create("KxMovie", DISPATCH_QUEUE_SERIAL);
        _videoFrames    = [NSMutableArray array];
        _audioFrames    = [NSMutableArray array];
        
        if (_decoder.subtitleStreamsCount) {
            _subtitles = [NSMutableArray array];
        }
        
        // useless
//        if (_decoder.isNetwork) {
//            
//            _minBufferedDuration = NETWORK_MIN_BUFFERED_DURATION;
//            _maxBufferedDuration = NETWORK_MAX_BUFFERED_DURATION;
//            
//        } else {
//            
//            _minBufferedDuration = LOCAL_MIN_BUFFERED_DURATION;
//            _maxBufferedDuration = LOCAL_MAX_BUFFERED_DURATION;
//        }
//        
//        if (!_decoder.validVideo)
//            _minBufferedDuration *= 10.0; // increase for audio
        
        // allow to tweak some parameters at runtime
        if (_parameters.count) {
            
            id val;
            
            val = [_parameters valueForKey: KxMovieParameterMinBufferedDuration];
            if ([val isKindOfClass:[NSNumber class]])
                _minBufferedDuration = [val floatValue];
            
            val = [_parameters valueForKey: KxMovieParameterMaxBufferedDuration];
            if ([val isKindOfClass:[NSNumber class]])
                _maxBufferedDuration = [val floatValue];
            
            val = [_parameters valueForKey: KxMovieParameterDisableDeinterlacing];
            if ([val isKindOfClass:[NSNumber class]])
                _decoder.disableDeinterlacing = [val boolValue];
            
            if (_maxBufferedDuration < _minBufferedDuration)
                _maxBufferedDuration = _minBufferedDuration * 2;
        }
        
        LoggerStream(2, @"buffered limit: %.1f - %.1f", _minBufferedDuration, _maxBufferedDuration);
        
        if (self.isViewLoaded) {
            [self setupPresentView];
            [self play];
        }
    }
    
    else {
        NSLog(@"Brosso: Set decoder failed");
    }
    
    // Reset state
    self.busying = NO;
}

- (void) setupPresentView {
//    // Fix background crash issue
//    UIApplicationState state = [[UIApplication sharedApplication] applicationState];
//    if (UIApplicationStateBackground == state || UIApplicationStateInactive == state) {
//        return;
//    }
    
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    
    if (_glView) return;
    
    CGRect bounds = [[UIScreen mainScreen] bounds];

    
    if (1) { // [Utility model]: check iphone device is new enough
        LoggerVideo(0, @"get rid of openGL, use RGB video frame and UIKit");
        [_decoder setupVideoFrameFormat:KxVideoFrameFormatRGB];
        _imageView = [[UIImageView alloc] initWithFrame:bounds];
        _imageView.backgroundColor = [UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1.0];
    }
    else {
        LoggerVideo(0, @"try openGL");
        if (_decoder.validVideo) {
            _glView = [[KxMovieGLView alloc] initWithFrame:bounds decoder:_decoder];
        }
        
        if (!_glView) {
            LoggerVideo(0, @"fallback to use RGB video frame and UIKit");
            [_decoder setupVideoFrameFormat:KxVideoFrameFormatRGB];
            _imageView = [[UIImageView alloc] initWithFrame:bounds];
            _imageView.backgroundColor = [UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1.0];
        }
    }
    
    [self frameViewHack];
    UIView *frameView = [self frameView];
    
    
    _scrollView = [[UIScrollView alloc] initWithFrame:bounds];
    _scrollView.backgroundColor = [UIColor redColor];
    
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.showsVerticalScrollIndicator = NO;
    _scrollView.scrollEnabled = YES;
    _scrollView.delegate = self;
    _scrollView.minimumZoomScale = 1.0f;
    _scrollView.maximumZoomScale = MAX_ZOOM_SCALE;
    _scrollView.decelerationRate = UIScrollViewDecelerationRateFast;
    [_scrollView addSubview:frameView];
    
//    [self.view insertSubview:_scrollView atIndex:0];
    [self.view addSubview:_scrollView];
    
    
    self.view.backgroundColor = [UIColor yellowColor];
    
    
    
    frameView.contentMode = _scrollView.contentMode = UIViewContentModeScaleAspectFit; //UIViewContentModeScaleAspectFill
    _scrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleRightMargin;
}

- (UIView *) frameView {
    return _glView ? _glView : _imageView;
}

- (void) audioCallbackFillData: (float *) outData
                     numFrames: (UInt32) numFrames
                   numChannels: (UInt32) numChannels
{
    //fillSignalF(outData,numFrames,numChannels);
    //return;
    
    if (_buffered) {
        memset(outData, 0, numFrames * numChannels * sizeof(float));
        return;
    }
    
    @autoreleasepool {
        
        while (numFrames > 0) {
            
            if (!_currentAudioFrame) {
                
                @synchronized(_audioFrames) {
                    
                    NSUInteger count = _audioFrames.count;
                    
                    if (count > 0) {
                        
                        KxAudioFrame *frame = _audioFrames[0];
                        
#ifdef DUMP_AUDIO_DATA
                        LoggerAudio(2, @"Audio frame position: %f", frame.position);
#endif
                        if (_decoder.validVideo) {
                            
                            const CGFloat delta = _moviePosition - frame.position;
                            
//                            if (delta < -0.1) { // while cause ifinite waiting after resume from background
//                            //if (delta < -0.3) {
//                                
////                                //memset(outData, 0, numFrames * numChannels * sizeof(float));
//#ifdef DEBUG
//                                LoggerStream(0, @"desync audio (outrun) wait %.4f %.4f", _moviePosition, frame.position);
//                                _debugAudioStatus = 1;
//                                _debugAudioStatusTS = [NSDate date];
//#endif
//
//                            }
                            
                            [_audioFrames removeObjectAtIndex:0]; // remove this audio frame no matter what
                            _moviePosition = frame.position;
                            _bufferedDuration -= frame.duration;
                            
                            if (delta > 0.1 && count > 1) {
//#ifdef DEBUG
//                                LoggerStream(0, @"desync audio (lags) skip %.4f %.4f", _moviePosition, frame.position);
//                                _debugAudioStatus = 2;
//                                _debugAudioStatusTS = [NSDate date];
//#endif
                                continue;
                            }
                            
                        } else {
                            
                            [_audioFrames removeObjectAtIndex:0];
                            _moviePosition = frame.position;
                            _bufferedDuration -= frame.duration;
                        }
                        
                        _currentAudioFramePos = 0;
                        _currentAudioFrame = frame.samples;
                    }
                }
            }
            
            if (_currentAudioFrame) {
                
                const void *bytes = (Byte *)_currentAudioFrame.bytes + _currentAudioFramePos;
                const NSUInteger bytesLeft = (_currentAudioFrame.length - _currentAudioFramePos);
                const NSUInteger frameSizeOf = numChannels * sizeof(float);
                const NSUInteger bytesToCopy = MIN(numFrames * frameSizeOf, bytesLeft);
                const NSUInteger framesToCopy = bytesToCopy / frameSizeOf;
                
                memcpy(outData, bytes, bytesToCopy);
                numFrames -= framesToCopy;
                outData += framesToCopy * numChannels;
                
                if (bytesToCopy < bytesLeft)
                    _currentAudioFramePos += bytesToCopy;
                else
                    _currentAudioFrame = nil;
                
            } else {
                
                memset(outData, 0, numFrames * numChannels * sizeof(float));
                //LoggerStream(1, @"silence audio");
//#ifdef DEBUG
//                _debugAudioStatus = 3;
//                _debugAudioStatusTS = [NSDate date];
//#endif
                break;
            }
        }
    }
}

- (void) enableAudio: (BOOL) on
{
    id<KxAudioManager> audioManager = [KxAudioManager audioManager];
    
    if (on && _decoder.validAudio) {
        
        audioManager.outputBlock = ^(float *outData, UInt32 numFrames, UInt32 numChannels) {
            
            [self audioCallbackFillData: outData numFrames:numFrames numChannels:numChannels];
        };
        
        [audioManager play];
        
        LoggerAudio(2, @"audio device smr: %d fmt: %d chn: %d",
                    (int)audioManager.samplingRate,
                    (int)audioManager.numBytesPerSample,
                    (int)audioManager.numOutputChannels);
        
    } else {
        
        [audioManager pause];
        audioManager.outputBlock = nil;
    }
}

- (BOOL) addFrames: (NSArray *)frames
{
    if (_decoder.validVideo) {
        
        @synchronized(_videoFrames) {
            for (KxMovieFrame *frame in frames)
                if (frame.type == KxMovieFrameTypeVideo) {
                    [_videoFrames addObject:frame];
                    _bufferedDuration += frame.duration;
                }
        }
    }
    
    if (_decoder.validAudio) {
        
        @synchronized(_audioFrames) {
            
            for (KxMovieFrame *frame in frames)
                if (frame.type == KxMovieFrameTypeAudio) {
                    [_audioFrames addObject:frame];
                    if (!_decoder.validVideo)
                        _bufferedDuration += frame.duration;
                }
        }
        
        if (!_decoder.validVideo) {
            
            for (KxMovieFrame *frame in frames)
                if (frame.type == KxMovieFrameTypeArtwork)
                    self.artworkFrame = (KxArtworkFrame *)frame;
        }
    }
    
    if (_decoder.validSubtitles) {
        
        @synchronized(_subtitles) {
            
            for (KxMovieFrame *frame in frames)
                if (frame.type == KxMovieFrameTypeSubtitle) {
                    [_subtitles addObject:frame];
                }
        }
    }
    
    return self.playing && _bufferedDuration < _maxBufferedDuration;
}

- (void) asyncDecodeFrames
{
    if (self.decoding)
        return;
    
    __weak HomeMadeLiveViewController *weakSelf = self;
    __weak KxMovieDecoder *weakDecoder = _decoder;
    
    const CGFloat duration = _decoder.isNetwork ? .0f : 0.1f;
    
    self.decoding = YES;
    
    dispatch_async(_dispatchQueue, ^{

        {
            __strong HomeMadeLiveViewController *strongSelf = weakSelf;
            if (!strongSelf.playing)
                return;
        }
        
        BOOL good = YES;
        while (good) {
            
            good = NO;
            
            @autoreleasepool {
                
                __strong KxMovieDecoder *decoder = weakDecoder;
                
                if (decoder && (decoder.validVideo || decoder.validAudio)) {
                    NSArray *frames = [decoder decodeFrames:duration];
                   
                    if (frames.count) {
                        __strong HomeMadeLiveViewController *strongSelf = weakSelf;
                        if (strongSelf)
                            good = [strongSelf addFrames:frames];
                    }
                }
            }
        }
        
        {
            __strong HomeMadeLiveViewController *strongSelf = weakSelf;
            if (strongSelf) strongSelf.decoding = NO;
        }
    });
}

- (void) tick {
    if (_buffered && ((_bufferedDuration > _minBufferedDuration) || _decoder.isEOF)) {
        _tickCorrectionTime = 0;
        _buffered = NO;
    }
    
    CGFloat interval = 0;
    if (!_buffered) {
        interval = [self presentFrame];
    }
    
    if (self.playing) {
        
        const NSUInteger leftFrames =
        (_decoder.validVideo ? _videoFrames.count : 0) +
        (_decoder.validAudio ? _audioFrames.count : 0);
        
        if (0 == leftFrames) {
            
            if (_decoder.isEOF) {
                [self pause];
                // Brosso: disconnect handle
                NSLog(@"STREAM_EOF (disconnect?)");
//                [[NSNotificationCenter defaultCenter] postNotificationName:STREAM_EOF object:nil];
                [self backgroundServiceNotify];
                return;
            }
            
            if (_minBufferedDuration > 0 && !_buffered) {
                
                _buffered = YES;

                // Brosso: hack - 50%
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone && arc4random()%2>0) { // 50% chance
                    //NSLog(@"Brosso: hack : post STREAM_EOF !!!");
//                    [[NSNotificationCenter defaultCenter] postNotificationName:STREAM_EOF object:nil];
                    [self backgroundServiceNotify];
                }
            }
        }
        
        if (!leftFrames ||
            !(_bufferedDuration > _minBufferedDuration)) {
            
            [self asyncDecodeFrames];
        }
        
        const NSTimeInterval correction = [self tickCorrection];
        const NSTimeInterval time = MAX(interval + correction, 0.01);
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, time * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self tick];
        });
    }
}

- (CGFloat) tickCorrection
{
    if (_buffered)
        return 0;
    
    const NSTimeInterval now = [NSDate timeIntervalSinceReferenceDate];
    
    if (!_tickCorrectionTime) {
        
        _tickCorrectionTime = now;
        _tickCorrectionPosition = _moviePosition;
        
        return 0;
    }
    
    NSTimeInterval dPosition = _moviePosition - _tickCorrectionPosition;
    NSTimeInterval dTime = now - _tickCorrectionTime;
    NSTimeInterval correction = dPosition - dTime;

    
    if (correction > 1.f || correction < -1.f) {

        
        LoggerStream(1, @"tick correction reset %.2f", correction);
//        LoggerStream(1, @"_videoFrames count: %d, _audioFrames count :%d, buffered: %d, _minBufferedDuration = %f",_videoFrames.count, _audioFrames.count, _buffered, _minBufferedDuration);
        correction = 0;
        _tickCorrectionTime = 0;
    }

    return correction;
}

- (CGFloat) presentFrame
{
    CGFloat interval = 0;
    
    if (_decoder.validVideo) {
        
        KxVideoFrame *frame;
        
        @synchronized(_videoFrames) {
            if (_videoFrames.count > 0) {
                frame = _videoFrames[0];
                [_videoFrames removeObjectAtIndex:0];
                _bufferedDuration -= frame.duration;
            }
        }
        
        if (frame) {
            interval = [self presentVideoFrame:frame];
        }
    } else if (_decoder.validAudio) {
        
        //interval = _bufferedDuration * 0.5;
        
        if (self.artworkFrame) {
            
            _imageView.image = [self.artworkFrame asImage];
            self.artworkFrame = nil;
        }
    }
    
    if (_decoder.validSubtitles)
        [self presentSubtitles];
    
//#ifdef DEBUG
//    if (self.playing && _debugStartTime < 0)
//        _debugStartTime = [NSDate timeIntervalSinceReferenceDate] - _moviePosition;
//#endif
    
    return interval;
}

- (CGFloat) presentVideoFrame: (KxVideoFrame *) frame {
    if (_backgroundMode) { // background audio mode skip gl rendering
        return frame.duration;
    }
    
    if (_recoverFromBackground) { // Brosso: force reload
        _recoverFromBackground = NO;
        if (self.playing) {
            [self pause];
//            [[NSNotificationCenter defaultCenter] postNotificationName:STREAM_EOF object:nil];
        }
        return frame.duration;
    }
    
    _currentPresentVideoFrame = frame;
    
    if (_glView) {
        
        [_glView render:frame];
        
    } else {
        
        KxVideoFrameRGB *rgbFrame = (KxVideoFrameRGB *)frame;
        _imageView.image = [rgbFrame asImage];
    }
    
    _moviePosition = frame.position;
    
    return frame.duration;
}

- (void) presentSubtitles {
    NSArray *actual, *outdated;
    
    if ([self subtitleForPosition:_moviePosition
                           actual:&actual
                         outdated:&outdated]){
        
        if (outdated.count) {
            @synchronized(_subtitles) {
                [_subtitles removeObjectsInArray:outdated];
            }
        }
        
//        if (actual.count) {
//            
//            NSMutableString *ms = [NSMutableString string];
//            for (KxSubtitleFrame *subtitle in actual.reverseObjectEnumerator) {
//                if (ms.length) [ms appendString:@"\n"];
//                [ms appendString:subtitle.text];
//            }
//            
//            if (![_subtitlesLabel.text isEqualToString:ms]) {
//                
//                CGSize viewSize = self.view.bounds.size;
//                CGSize size = [ms sizeWithFont:_subtitlesLabel.font
//                             constrainedToSize:CGSizeMake(viewSize.width, viewSize.height * 0.5)
//                                 lineBreakMode:NSLineBreakByTruncatingTail];
//                _subtitlesLabel.text = ms;
//                _subtitlesLabel.frame = CGRectMake(0, viewSize.height - size.height - 10,
//                                                   viewSize.width, size.height);
//                _subtitlesLabel.hidden = NO;
//            }
//            
//        } else {
//            
//            _subtitlesLabel.text = nil;
//            _subtitlesLabel.hidden = YES;
//        }
    }
}

- (BOOL) subtitleForPosition: (CGFloat) position
                      actual: (NSArray **) pActual
                    outdated: (NSArray **) pOutdated
{
    if (!_subtitles.count)
        return NO;
    
    NSMutableArray *actual = nil;
    NSMutableArray *outdated = nil;
    
    for (KxSubtitleFrame *subtitle in _subtitles) {
        
        if (position < subtitle.position) {
            
            break; // assume what subtitles sorted by position
            
        } else if (position >= (subtitle.position + subtitle.duration)) {
            
            if (pOutdated) {
                if (!outdated)
                    outdated = [NSMutableArray array];
                [outdated addObject:subtitle];
            }
            
        } else {
            
            if (pActual) {
                if (!actual)
                    actual = [NSMutableArray array];
                [actual addObject:subtitle];
            }
        }
    }
    
    if (pActual) *pActual = actual;
    if (pOutdated) *pOutdated = outdated;
    
    return actual.count || outdated.count;
}

- (void) freeBufferedFrames
{
    @synchronized(_videoFrames) {
        [_videoFrames removeAllObjects];
    }
    
    @synchronized(_audioFrames) {
        
        [_audioFrames removeAllObjects];
        _currentAudioFrame = nil;
    }
    
    if (_subtitles) {
        @synchronized(_subtitles) {
            [_subtitles removeAllObjects];
        }
    }
    
    _bufferedDuration = 0;
}

#pragma mark - Scroll view delegate
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return [self frameView];
}

- (void)frameViewHack {
    // Restore zoom first
    [self restoreZoom];
    
    // Magic rect
    CGRect frame = [[UIScreen mainScreen] bounds];
    CGFloat oldWidth  = frame.size.width;
    CGFloat oldHeight = frame.size.height;

    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    BOOL isPortrait = UIInterfaceOrientationIsPortrait(orientation);
    
    CGFloat newWidth = (isPortrait)? MIN(oldWidth, oldHeight): MAX(oldWidth, oldHeight);
    CGFloat newHeight = (isPortrait)? MAX(oldWidth, oldHeight) * 0.8: MIN(oldWidth, oldHeight);
    CGRect newRect = CGRectMake(0, 0, newWidth, newHeight);
    
    [[self frameView] setFrame:newRect];
    [_scrollView setContentSize:CGSizeMake(newWidth, newHeight)];
}

- (void)restoreZoom {
    [_scrollView setZoomScale:1.0 animated:YES];
}

- (void)backgroundServiceNotify {
    if (_backgroundMode) {
//        NSLog(@"Audio Disconnected/Refresh");
        _decoder = nil;
//        [self loadContentWithIndex:_pageIndex]; // audio resume
    }
}


@end
