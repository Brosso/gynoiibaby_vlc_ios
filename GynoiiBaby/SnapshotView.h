//
//  SnapshotView.h
//  Babycam
//
//  Created by BingHuan Wu on 4/24/17.
//  Copyright © 2017 Gynoii. All rights reserved.
//

#define TIMELAPSE_GAP1 10 * 60 * 2
#define TIMELAPSE_GAP2 20 * 60 * 2
#define TIMELAPSE_GAP3 40 * 60 * 2
#define TIMELAPSE_GAP4 80 * 60 * 2
#define TIMELAPSE_GAP5 160 * 60 * 2

#import <UIKit/UIKit.h>

@interface SnapshotView : UIView

- (void)setParentView:(UIView*)parent;
- (void)startUpdate;
- (void)stopUpdate;
- (BOOL)timelapseDidTouch:(NSInteger)index;

@end
