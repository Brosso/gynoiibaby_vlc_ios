//
//  TabBarViewController.m
//  GynoiiBaby
//
//  Created by BingHuan Wu on 3/14/17.
//  Copyright © 2017 Gynoii. All rights reserved.
//

#import "TabBarViewController.h"

@interface TabBarViewController ()

@end

@implementation TabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)setupUI {
    NSArray *titleArray = @[NSLocalizedString(@"Live View", nil), NSLocalizedString(@"Gallery", nil), NSLocalizedString(@"Option", nil), NSLocalizedString(@"Blog", nil)];
    NSArray *imageArray = @[[UIImage imageNamed:@"ic_tab_liveview"], [UIImage imageNamed:@"ic_tab_gallery"], [UIImage imageNamed:@"ic_tab_option"], [UIImage imageNamed:@"ic_tab_news"]];
    
    for (NSInteger index = 0; index < [[self viewControllers] count]; index ++) {
        UIViewController *vc = [[self viewControllers] objectAtIndex:index];
        UITabBarItem *item = [[UITabBarItem alloc] initWithTitle:[titleArray objectAtIndex:index] image:[imageArray objectAtIndex:index] tag:index];
        [vc setTabBarItem:item];
    }
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

@end
