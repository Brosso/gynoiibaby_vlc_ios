//
//  CameraManager.h
//  GynoiiBaby
//
//  Created by BingHuan Wu on 3/13/17.
//  Copyright © 2017 Gynoii. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SearchIPCamWrapper.h"

// Development flag
#define FORCE_P2P

// Camera info dictionary keys
#define CAMERA_LIST     @"kCAMERA_LIST"      // Array of dictionaries contains MAC, UID, NAME, and other info
#define CAMERA_UID      @"kCAMERA_UID"       // UID of a camera
#define CAMERA_MAC      @"kCAMERA_MAC"       // MAC of a camera
#define CAMERA_NAME     @"kCAMERA_NAME"      // Name of a camera
#define CAMERA_STATUS   @"kCAMERA_STATUS"    // Flag to show if camera is online
#define SAT_VALID       @"kSAT_VALID"        // Flag to show if P2P path is valid
#define WAN_VALID       @"kWAN_VALID"        // Flag to show if wan port is valid
#define TALK_VALID      @"kTALK_VALID"       // Flag to show if talk port is valid
#define SAT_PATH        @"kSAT_PATH"         // Strem URL prefix after P2P connection done
#define SNAPSHOT_PATH   @"kSNAPSHOT_PATH"    // URL suffix for taking snapshot
#define SAT_PORT        @"kSAT_PORT"         // RTSP port -> always 554
#define RTSP_PORT       @"kRTSP_PORT"        // RTSP port after P2P connection done -> map to 554
#define WAN_PORT        @"kWAN_PORT"         // CGI port after P2P connection done -> map to 80
#define TALK_PORT       @"kTALK_PORT"        // Two way talk port after P2P connection done -> map to 50000
#define STREAM_PATH     @"kSTREAM_PATH"      // URL suffix for current camera stream (default to low quality)
#define STREAM_PATH_LOW @"kSTREAM_PATH_LOW"  // URL suffix for low quality camera stream
#define STREAM_PATH_MID @"kSTREAM_PATH_MID"  // URL suffix for mid quality camera stream
#define STREAM_PATH_HIGH @"kSTREAM_PATH_HIGH"// URL suffix for high quality camera stream
#define LAN_VALID       @"kLAN_VALID"        // Flag to show if LAN path detected
#define LAN_IP          @"kLAN_IP"           // LAN IP string


// Constants
#define RTSP_PORT_NUMBER    (554)
#define WAN_PORT_NUMBER     (80)
#define TALK_PORT_NUMBER    (50000)
#define CAMERA_NOT_FOUND    (@-1)             // Cannot find camera
#define LOCAL_HOST_IP       (@"127.0.0.1")

// Notification name
#define LIST_UPDATED_NOTIFICATION   @"kLIST_UPDATED_NOTIFICATION"   // Post whenever camera list get updated


@interface CameraManager : NSObject
@property (strong, nonatomic) NSMutableArray* cameraList;

+ (id)sharedInstance;
+ (void)setCamCameraList:(NSMutableArray*)newCameraList;
+ (NSMutableArray*)getCameraList;
+ (void)restoreCameraListFromDefaults;
+ (void)syncCameraListToDefaults;
+ (NSInteger)findCameraIndexWithUID:(NSString*)UID;
+ (NSInteger)findCameraIndexWithMAC:(NSString*)MAC;
+ (void)updateCamera:(NSString*)UID withObject:(id)object andKey:(NSString*)key;
+ (void)updateCameraMAC:(NSString*)MAC withObject:(id)object andKey:(NSString*)key;
+ (void)batchUpdateCamera:(NSMutableDictionary*)cameraInfo;
+ (void)resetCameraValidation;

// LAN Search
+ (void)broadcastLAN;

@end
