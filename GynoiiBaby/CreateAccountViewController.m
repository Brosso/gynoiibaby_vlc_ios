//
//  CreateAccountViewController.m
//  GynoiiBaby
//
//  Created by BingHuan Wu on 3/15/17.
//  Copyright © 2017 Gynoii. All rights reserved.
//

#import "CreateAccountViewController.h"

@interface CreateAccountViewController ()

@end

@implementation CreateAccountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // Tap dismiss
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    
    // Setup UI
    [self setupUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dismissKeyboard {
    [username resignFirstResponder];
    [password resignFirstResponder];
    [confirmPassword resignFirstResponder];
    [email resignFirstResponder];
}

- (void)setupUI {
    // Custom spinner
    [spinner setTransform:CGAffineTransformMakeScale(1.5, 1.5)];
    [spinner setColor:GYNOII_GREEN];
    
    // TODO: localization
}

-(IBAction)createAccount:(id)sender {
    [self dismissKeyboard];
    [spinner setHidden:NO];
    
    // Format check
    if (![Utility isValidQlyncFormat:username.text]) {
        [Utility makeWarningToastWithTitle:NSLocalizedString(@"Invalid format", nil) andMessage:NSLocalizedString(@"Please enter 4~32 alphabet or numeric characters", nil)];
        [spinner setHidden:YES];
        return;
    }
    
    if (![Utility isValidQlyncFormat:password.text]) {
        [Utility makeWarningToastWithTitle:NSLocalizedString(@"Invalid format", nil) andMessage:NSLocalizedString(@"Please enter 4~32 alphabet or numeric characters", nil)];
        [spinner setHidden:YES];
        return;
    }
    
    if (![confirmPassword.text isEqualToString:password.text]) {
        [Utility makeWarningToastWithTitle:NSLocalizedString(@"Error", nil) andMessage:NSLocalizedString(@"Please confirm your password", nil)];
        [spinner setHidden:YES];
        return;
    }
    
    if (![Utility isValidEmail:email.text]) {
        [Utility makeWarningToastWithTitle:NSLocalizedString(@"Error", nil) andMessage:NSLocalizedString(@"Please provide a valid Email address", nil)];
        [spinner setHidden:YES];
        return;
    }
    
    // Format check pass, make URL request to qlync
    NSString *content = [NSString stringWithFormat:@"%@NUUOLINK%@23641580%@",username.text,password.text,email.text];
    content = [Utility MD5:content];
    NSString *requestString = [NSString stringWithFormat:@"http://sat.qlync.com:80/backstage_user_register.php?command=mobileadd&content=%@&name=%@&pwd=%@&reg_email=%@&oem_id=%@&platform=iOS",content,username.text,password.text,email.text,@"G06"];
    
    void (^callBack)(NSURLResponse* response, NSData* data, NSError* connectionError) = ^(NSURLResponse* response, NSData* data, NSError* error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [spinner setHidden:YES];
        });
        
        if ([data length] >0 && error == nil) {
            NSError *err = nil;
            NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&err];
            
            if (!err) {
                NSString *status = [jsonDict objectForKey:@"status"];
                NSString *msg = [jsonDict objectForKey:@"error_msg"];
                if ([status isEqualToString:@"fail"]) {
                    
                    if ([msg hasPrefix:@"Name already"]) {
                        msg = NSLocalizedString(@"This account is already in use", nil);
                    }
                    else if ([msg hasPrefix:@"E-mail is"]) {
                        msg = NSLocalizedString(@"This Email address is already in use", nil);
                    }
                    
                    [Utility showAlertWithTitle:NSLocalizedString(@"Failure", nil) andMessage:msg];
                }
                else if ([status isEqualToString:@"success"]) {
                    [Utility showAlertWithTitle:NSLocalizedString(@"Success", nil) andMessage:NSLocalizedString(@"Please login with your new account", nil)];
                    [AppSetting updateAppSetting:USER_EMAIL withNewObject:email.text];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.navigationController popToRootViewControllerAnimated:YES];
                    });
                }
            }
        }
        else if ([data length] == 0 && error == nil) {
            [Utility makeWarningToastWithTitle:NSLocalizedString(@"Error", nil) andMessage:NSLocalizedString(@"Please try again later", nil)];
        }
        else if (error != nil) {
            [Utility makeWarningToastWithTitle:NSLocalizedString(@"Error", nil) andMessage:[error localizedDescription]];
        }
    };
    
    [Utility makeURLRequest:requestString withCallback:callBack];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark UI delegation
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

@end
