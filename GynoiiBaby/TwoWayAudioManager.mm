//
//  TwoWayAudioManager.m
//  Babycam
//
//  Created by Brosso on 9/1/14.
//  Copyright (c) 2014 Gynoii. All rights reserved.
//

#import "TwoWayAudioManager.h"

@implementation TwoWayAudioManager
@synthesize recorder;
@synthesize isRecording;
@synthesize isPlaying;

- (void)stopRecord {
	recorder->StopRecord();
    
	// Create a new queue for the recorded file
	recordFilePath = (__bridge CFStringRef)[NSTemporaryDirectory() stringByAppendingPathComponent: @"recordedFile.caf"];
}

- (void)twoWayTalkWithIP:(NSString*)ip andPort:(int)port {
	// If we are currently recording, stop and save the file.
    if (recorder->IsRunning()) {
		[self stopRecord];
        isRecording = NO;
	}
    // If we're not recording, start.
	else if (ip) {
        // Start the recorder
        recorder->ip = (char*)[ip UTF8String];
        recorder->port = port;
        recorder->StartRecord(CFSTR("recordedFile.caf"));
        isRecording = YES;
	}
}

- (void)twoWayMusicWithIP: (NSString*)ip port:(int)port musicIndex:(int)index {
    // Stop music playback first
    if (recorder->IsPlaying()) {
        recorder->StopMusic();
    }
    
    // Start music playback
    else if (ip && index) {
        recorder->ip = (char*)[ip UTF8String];
        recorder->port = port;
        recorder->PlayMusic([self getFilePath:index]);
        isPlaying = recorder->IsPlaying();
    }
}

- (BOOL)getPlayState {
    return (BOOL)(recorder->IsPlaying());
}

// Return file path for default / custom music
- (NSString *)getFilePath:(int) index {
    switch (index) {
        case 1: {
            return [[NSBundle mainBundle] pathForResource:@"Sleepy_Box" ofType:@"raw"];
        }
        case 2: {
            return [[NSBundle mainBundle] pathForResource:@"Musical_Box" ofType:@"raw"];
        }
        case -1: {
            return [[NSBundle mainBundle] pathForResource:@"doorbell" ofType:@"raw"];
        }
    }
    
    index -=2;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSArray *directoryContent = [fileManager contentsOfDirectoryAtPath:documentsDirectory error:NULL];
    
    for (int count = 0; count < (int)[directoryContent count]; count++) {
        NSString * name = [directoryContent objectAtIndex:count];
        if([name hasSuffix:@"ulaw"]){
            if(--index == 0)
                return [NSString stringWithFormat:@"%@/%@",documentsDirectory,name ];
        }
    }
    
    return nil;
}

#pragma mark AudioSession listeners
void interruptionListener(	void *	inClientData,
                          UInt32	inInterruptionState)
{
	TwoWayAudioManager *THIS = (__bridge TwoWayAudioManager*)inClientData;
	if (inInterruptionState == kAudioSessionBeginInterruption)
	{
		if (THIS->recorder->IsRunning()) {
			[THIS stopRecord];
		}
	}
}

void propListener(void *                    inClientData,
                  AudioSessionPropertyID    inID,
                  UInt32                    inDataSize,
                  const void *              inData)
{
	TwoWayAudioManager *THIS = (__bridge TwoWayAudioManager*)inClientData;
	if (inID == kAudioSessionProperty_AudioRouteChange)
	{
		CFDictionaryRef routeDictionary = (CFDictionaryRef)inData;
		//CFShow(routeDictionary);
		CFNumberRef reason = (CFNumberRef)CFDictionaryGetValue(routeDictionary, CFSTR(kAudioSession_AudioRouteChangeKey_Reason));
		SInt32 reasonVal;
		CFNumberGetValue(reason, kCFNumberSInt32Type, &reasonVal);
		if (reasonVal != kAudioSessionRouteChangeReason_CategoryChange)
		{
			// stop the queue if we had a non-policy route change
			if (THIS->recorder->IsRunning()) {
				[THIS stopRecord];
			}
		}
	}
}

- (id) init {
    if (self = [super init]) {
        recorder = new AQRecorder();
        isRecording = NO;
        isPlaying = NO;
        
        [self registerForBackgroundNotifications];
    }
    return self;
}

#pragma mark background notifications
- (void)registerForBackgroundNotifications {
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(resignActive)
												 name:UIApplicationWillResignActiveNotification
											   object:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(enterForeground)
												 name:UIApplicationWillEnterForegroundNotification
											   object:nil];
}

- (void)resignActive {
    if (recorder->IsRunning()) [self stopRecord];
}

- (void)enterForeground {
    OSStatus error = AudioSessionSetActive(true);
    if (error) {
        printf("AudioSessionSetActive (true) failed");
    }
}

#pragma mark Cleanup
- (void)dealloc {
	delete recorder;
}

@end
