//
//  ResetPasswordViewController.h
//  GynoiiBaby
//
//  Created by BingHuan Wu on 3/15/17.
//  Copyright © 2017 Gynoii. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResetPasswordViewController : UIViewController <UITextFieldDelegate> {
    IBOutlet UITextField *nameOrEmail;
    IBOutlet UIActivityIndicatorView *spinner;
}

@end
