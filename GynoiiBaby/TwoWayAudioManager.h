//
//  TwoWayAudioManager.h
//  Babycam
//
//  Created by Brosso on 9/1/14.
//  Copyright (c) 2014 Gynoii. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AQRecorder.h"

#define MAX_PLAYER_NUM 10

@interface TwoWayAudioManager : NSObject
{
    AQRecorder *recorder;
	CFStringRef recordFilePath;
    BOOL isRecording;
    BOOL isPlaying;
    AQRecorder *musicPlayerList[MAX_PLAYER_NUM];
}

@property BOOL isRecording;
@property BOOL isPlaying;
@property (readonly) AQRecorder	*recorder;

- (void)twoWayTalkWithIP:(NSString*)ip andPort:(int)port;
- (void)twoWayMusicWithIP: (NSString*)ip port:(int)port musicIndex:(int)index;
- (BOOL)getPlayState;
@end
