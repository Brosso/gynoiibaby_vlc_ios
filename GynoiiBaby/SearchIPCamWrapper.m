//
//  SearchIPCamWrapper.m
//  Babycam
//
//  Created by BingHuan Wu on 12/31/15.
//  Copyright © 2015 Gynoii. All rights reserved.
//

#import "SearchIPCamWrapper.h"

@implementation SearchIPCamWrapper

+ (id)sharedInstance {
    // structure used to test whether the block has completed or not
    static dispatch_once_t p = 0;
    
    // initialize sharedObject as nil (first call only)
    __strong static id _sharedObject = nil;
    
    // executes a block object once and only once for the lifetime of an application
    dispatch_once(&p, ^{
        _sharedObject = [[self alloc] init];
    });
    
    // returns the same object each time
    return _sharedObject;
}

- (id)init {
    if ((self = [super init])) {
        search = [[SearchIPCam alloc]init];
        [search startSearch];
    }
    return self;
}

- (NSArray*)getAllInfo {
    return [search getAllInfo];
}

@end
