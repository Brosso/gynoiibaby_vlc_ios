//
//  CustomMusicTableViewController.m
//  GynoiiBaby
//
//  Created by BingHuan Wu on 25/05/2017.
//  Copyright © 2017 Gynoii. All rights reserved.
//

#import "CustomMusicTableViewController.h"

extern void ThreadStateInitalize();
extern OSStatus DoConvertFile(CFURLRef sourceURL, CFURLRef destinationURL, OSType outputFormat, Float64 outputSampleRate);

@interface CustomMusicTableViewController () {
    NSMutableArray *list;
    UIBarButtonItem *editAction;
    UIBarButtonItem *doneAction;
    UIBarButtonItem *addAction;
}

@end

@implementation CustomMusicTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    editAction = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Edit", nil) style:UIBarButtonItemStylePlain target:self action:@selector(editClick)];
    doneAction = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", nil) style:UIBarButtonItemStylePlain target:self action:@selector(doneClick)];
    addAction = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Add", nil) style:UIBarButtonItemStylePlain target:self action:@selector(addNewSong)];

    list = [NSMutableArray array];
    
    [self doneClick];
    [self updateList];
    
    // Converter thread
    ThreadStateInitalize();
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO];
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES];
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Private
- (void)editClick {
    [self.tableView setEditing:YES animated:YES];
    self.navigationItem.rightBarButtonItems = nil;
    self.navigationItem.rightBarButtonItem = doneAction;
}

- (void)doneClick {
    self.navigationItem.rightBarButtonItems = @[editAction, addAction];
    [self.tableView setEditing:NO animated:YES];
}

-(void)updateList {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSArray *directoryContent = [fileManager contentsOfDirectoryAtPath:documentsDirectory error:NULL];
    
    for (int count = 0; count < (int)[directoryContent count]; count++) {
        NSString * name = [directoryContent objectAtIndex:count];
        if([name hasSuffix:@"ulaw"]){
            [list addObject:name];
        }
    }
}

-(void) addNewSong {
    MPMediaLibraryAuthorizationStatus authorizationStatus = [MPMediaLibrary authorizationStatus];
    if (MPMediaLibraryAuthorizationStatusAuthorized != authorizationStatus) {
        [self promptPermissionWithMessage:NSLocalizedString(@"Gynoii Baby has no access to media library", nil)];
        return;
    }
    
    MPMediaPickerController *mediaPicker = [[MPMediaPickerController alloc] initWithMediaTypes: MPMediaTypeMusic];
    mediaPicker.delegate = self;
    mediaPicker.allowsPickingMultipleItems = NO;
    [mediaPicker loadView];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:mediaPicker animated:YES completion:nil];
    });
}

- (void)promptPermissionWithMessage:(NSString*)message {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning", nil) message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleDestructive handler:nil];
    UIAlertAction *setting = [UIAlertAction actionWithTitle:NSLocalizedString(@"App Settings", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (UIApplicationOpenSettingsURLString) {
            NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
            [[UIApplication sharedApplication] openURL:url];
        }
        else {
            // Present some dialog telling the user to open the settings app.
            [Utility makeWarningToastWithTitle:NSLocalizedString(@"Warning", nil) andMessage:NSLocalizedString(@"Please authorize Gynoii Baby in Privacy Settings", nil)];
        }
    }];
    
    [alert addAction:cancel];
    [alert addAction:setting];
    
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark Media picker delegate
- (void)mediaPicker:(MPMediaPickerController *)mediaPicker didPickMediaItems:(MPMediaItemCollection *)mediaItemCollection {
    [mediaPicker dismissViewControllerAnimated:NO completion:nil];
    
    // Retrive media item
    MPMediaItem *mediaItem = [[mediaItemCollection items] objectAtIndex:0];
    NSString* soundName = [mediaItem valueForProperty:MPMediaItemPropertyTitle];
    NSURL *soundUrl = [mediaItem valueForProperty:MPMediaItemPropertyAssetURL];
    
    AVURLAsset *songAsset = [AVURLAsset URLAssetWithURL: soundUrl options:nil];
    AVAssetExportSession *exporter = [[AVAssetExportSession alloc]
                                      initWithAsset: songAsset
                                      presetName: AVAssetExportPresetPassthrough];
    //NSLog (@"created exporter. supportedFileTypes: %@", exporter.supportedFileTypes);
    exporter.outputFileType = @"com.apple.coreaudio-format";
    
    // stupid U2 album
    NSArray *types = exporter.supportedFileTypes;
    
    if (nil == exporter.supportedFileTypes) {
        [Utility makeWarningToastWithTitle:NSLocalizedString(@"Error", nil) andMessage:NSLocalizedString(@"Does not support free U2 album. Please select other music", nil)];
        return;
    }
    
    [Utility makeInfoToastWithTitle:NSLocalizedString(@"Encode start", nil) andMessage:NSLocalizedString(@"Your music is processing", nil)];
    
    // To prevent multiple encode crash
    [addAction setEnabled:NO];
    
    // Update list
    [list addObject:soundName];
    [self.tableView reloadData];
    
    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *destinationFilePath = [NSString stringWithFormat:@"%@/song.pcm", documentsDirectory];
    CFURLRef destinationURL = CFURLCreateWithFileSystemPath(kCFAllocatorDefault, (CFStringRef)destinationFilePath, kCFURLPOSIXPathStyle, false);
    NSString *destinationFilePath2 = [NSString stringWithFormat:@"%@/%@.ulaw", documentsDirectory,soundName];
    CFURLRef destinationURL2 = CFURLCreateWithFileSystemPath(kCFAllocatorDefault, (CFStringRef)destinationFilePath2, kCFURLPOSIXPathStyle, false);
    
    NSString *exportpath = [NSString stringWithFormat:@"%@/song.caf", documentsDirectory];
    exporter.outputURL = [NSURL fileURLWithPath:exportpath];
    [[NSFileManager defaultManager] removeItemAtURL:exporter.outputURL error:nil];
    
    CFURLRef sourceURL = CFURLCreateWithString(kCFAllocatorDefault, (CFStringRef)exportpath, NULL);
    
    [exporter exportAsynchronouslyWithCompletionHandler:^{
        AVAssetExportSessionStatus exportStatus = exporter.status;
        switch (exportStatus) {
            case AVAssetExportSessionStatusFailed: {
                NSError *exportError = exporter.error;
                NSLog (@"AVAssetExportSessionStatusFailed: %@", exportError);
                dispatch_barrier_async(dispatch_get_main_queue(), ^{
                    [addAction setEnabled:YES];
                });
                break;
            }
            case AVAssetExportSessionStatusCompleted: {
                NSLog (@"AVAssetExportSessionStatusCompleted");
                
                // First pass
                OSStatus error = DoConvertFile(sourceURL, destinationURL, kAudioFormatLinearPCM, 44100);
                if (error) {
                    NSError *err = [NSError errorWithDomain:NSOSStatusErrorDomain
                                                       code:error
                                                   userInfo:nil];
                    NSLog(@"convert LPCM error(%@)", [err localizedDescription]);
                    
                    dispatch_barrier_async(dispatch_get_main_queue(), ^{
                        [addAction setEnabled:YES];
                    });
                    return;
                }
                
                // Second pass
                error = DoConvertFile(destinationURL, destinationURL2, kAudioFormatULaw, 8000);
                if (error) {
                    NSError *err = [NSError errorWithDomain:NSOSStatusErrorDomain
                                                       code:error
                                                   userInfo:nil];
                    NSLog(@"convert ULAW error(%@)", [err localizedDescription]);
                    
                    dispatch_barrier_async(dispatch_get_main_queue(), ^{
                        [addAction setEnabled:YES];
                    });
                    return;
                }
                
                NSData *data = [[NSFileManager defaultManager] contentsAtPath:destinationFilePath2];
                NSData *d1 = [data subdataWithRange:NSMakeRange(5000, [data length] -5000)];
                [d1 writeToFile:destinationFilePath2 atomically:YES];
                
                dispatch_barrier_async(dispatch_get_main_queue(), ^{
                    [addAction setEnabled:YES];
                });
                
                [Utility makeInfoToastWithTitle:NSLocalizedString(@"Encode done", nil) andMessage:NSLocalizedString(@"Your music is ready now, tap music button to select and play", nil)];

                break;
            }
        }
    }];
}

- (void) mediaPickerDidCancel:(MPMediaPickerController *)mediaPicker{
    dispatch_barrier_async(dispatch_get_main_queue(), ^{
        [addAction setEnabled:YES];
        [mediaPicker dismissViewControllerAnimated:NO completion:nil];
    });
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [list count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"customMusicCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.textLabel.text = [[list objectAtIndex:indexPath.row] stringByDeletingPathExtension];
    
    return cell;
}



// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)aTableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Detemine if it's in editing mode
    if (self.tableView.editing) {
        return UITableViewCellEditingStyleDelete;
    }
    return UITableViewCellEditingStyleNone;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSString *name = [list objectAtIndex:indexPath.row];
        NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *destinationFilePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,name];
        
        [[NSFileManager defaultManager] removeItemAtPath:destinationFilePath error:nil];
        
        [list removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationRight];
    }
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
