//
//  AppDelegate.m
//  GynoiiBaby
//
//  Created by BingHuan Wu on 3/7/17.
//  Copyright © 2017 Gynoii. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    // UI appearance
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        [[UINavigationBar appearance] setBarTintColor:GYNOII_GREEN];
        [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
        
        [[UITabBar appearance] setTintColor:GYNOII_GREEN];
    }
    // Old style
    else {
        [[UINavigationBar appearance] setTintColor:GYNOII_GREEN];
        if ([[UITabBar appearance] respondsToSelector:@selector(setSelectedImageTintColor:)]) {
            [[UITabBar appearance] setSelectedImageTintColor:GYNOII_GREEN];
        }
    }
    
    [[UITabBar appearance] setTranslucent:NO];
    [[UINavigationBar appearance] setTitleTextAttributes: @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    // Initialize app setting
    [AppSetting initializeAppSetting];
    
    // Google login
    NSError* configureError;
    [[GGLContext sharedInstance] configureWithError: &configureError];
    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    
    [GIDSignIn sharedInstance].delegate = self;
    
    return YES;
}

- (BOOL)application:(UIApplication *)app
            openURL:(NSURL *)url
            options:(NSDictionary *)options {
    return [[GIDSignIn sharedInstance] handleURL:url
                               sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                      annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    return [[GIDSignIn sharedInstance] handleURL:url
                               sourceApplication:sourceApplication
                                      annotation:annotation];
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark Google login delegate
// The sign-in flow has finished and was successful if |error| is |nil|.
- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    
    //    NSString *userId = user.userID;                  // For client-side use only!
    //    NSString *idToken = user.authentication.idToken; // Safe to send to the server
    NSString *accessToken = user.authentication.accessToken; // Safe to send to the server
    //    NSString *fullName = user.profile.name;
    //    NSString *givenName = user.profile.givenName;
    //    NSString *familyName = user.profile.familyName;
    //    NSString *email = user.profile.email;
    //    NSLog(@"google sign in with token: %@",accessToken);
    if (error) {
        [Utility makeWarningToastWithTitle:NSLocalizedString(@"Login Error", nil) andMessage:[error localizedDescription]];
        [[NSNotificationCenter defaultCenter] postNotificationName:GOOGLE_LOGIN_FAIL object:nil userInfo:nil];
    }
    else {
        NSMutableDictionary *param = [NSMutableDictionary dictionary];
        [param setObject:@"G06" forKey:@"oem_id"];
        [param setObject:accessToken forKey:@"access_token"];
        
        void (^callBack)(NSURLResponse* response, NSData* data, NSError* connectionError) = ^(NSURLResponse* response, NSData* data, NSError* error) {
            if ([data length] > 0 && error == nil) {
                NSError *err = nil;
                NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&err];
                
                if (!err) {
                    NSString *username = [jsonDict objectForKey:@"username"];
                    NSString *password = [jsonDict objectForKey:@"pwd"];
                    NSString *email = [jsonDict objectForKey:@"email"];
                    NSString *errMsg = [jsonDict objectForKey:@"error_msg"];
                    
                    // Update google login status
                    if (username && password) {
                        [AppSetting updateAppSetting:USER_GOOGLE_LOGIN withNewObject:@1];
                        // Update email
                        if (email) {
                            [AppSetting updateAppSetting:USER_EMAIL withNewObject:email];
                        }
                        
                        // Post notification to login view controller
                        [[NSNotificationCenter defaultCenter] postNotificationName:GOOGLE_LOGIN_SUCCESS object:nil userInfo:jsonDict];
                    }
                    else {
                        // Qlync exchange fail
                        [[NSNotificationCenter defaultCenter] postNotificationName:GOOGLE_LOGIN_FAIL object:nil userInfo:nil];
                        [Utility makeWarningToastWithTitle:NSLocalizedString(@"Login Error", nil) andMessage:errMsg];
                        
                        // Google sign out
                        GIDSignIn *signOut = [GIDSignIn sharedInstance];
                        [signOut signOut];
                    }
                }
            }
            else { // server dead or any other unknown issue
#ifdef DEBUG
                [Utility makeWarningToastWithTitle:@"" andMessage:@"Google login failed with unknown issue"];
#endif
            }
        };
        
        [Utility makeURLPostRequest:@"https://sat.qlync.com/google_oauth.php" withParams:param andCallback:callBack];
    }
}


// Finished disconnecting |user| from the app successfully if |error| is |nil|.
- (void)signIn:(GIDSignIn *)signIn
didDisconnectWithUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    
    // Perform any operations when the user disconnects from app here.
    // ...
}

@end
