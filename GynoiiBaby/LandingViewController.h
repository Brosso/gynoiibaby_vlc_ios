//
//  LandingViewController.h
//  GynoiiBaby
//
//  Created by BingHuan Wu on 3/14/17.
//  Copyright © 2017 Gynoii. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Google/SignIn.h>

@interface LandingViewController : UIViewController <UITextFieldDelegate, GIDSignInUIDelegate> {
    IBOutlet UITextField    *username;
    IBOutlet UITextField    *password;
    IBOutlet UIButton       *loginBtn;
    IBOutlet UIButton       *googleLoginBtn;
    IBOutlet UIButton       *forgetBtn;
    IBOutlet UIButton       *createBtn;
    IBOutlet UILabel        *signinLbl;
    
    IBOutlet UILabel        *statusLbl;
    IBOutlet UIView         *loadingView;
    
    IBOutlet UIActivityIndicatorView *spinner;
}

@end
