//
//  Util.h
//  SI201
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonDigest.h>

@interface Util : NSObject

+ (NSString *)getMacAddress;
+ (NSString*)MD5: (NSString*)input;

@end
