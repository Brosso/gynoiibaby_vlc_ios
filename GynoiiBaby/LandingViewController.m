//
//  LandingViewController.m
//  GynoiiBaby
//
//  Created by BingHuan Wu on 3/14/17.
//  Copyright © 2017 Gynoii. All rights reserved.
//

#import "LandingViewController.h"

@interface LandingViewController () {
    // Work around flag
    BOOL isGoogleSignIn;
}

@end

@implementation LandingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // Tap gesture
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [tap setNumberOfTapsRequired:1];
    [self.view addGestureRecognizer:tap];
    
    // Custom spinner
    [spinner setTransform:CGAffineTransformMakeScale(1.5, 1.5)];
    [spinner setColor:GYNOII_GREEN];
    
    // Setup UI
    [self setupUI];
    
    // Google login
    [GIDSignIn sharedInstance].uiDelegate = self;
    //    [[GIDSignIn sharedInstance] signInSilently]; // Google automatically sign in the user
    
    // Notification observer
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(googleLoginProcess:) name:GOOGLE_LOGIN_SUCCESS object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(errorGoogleLogin:) name:GOOGLE_LOGIN_FAIL object:nil];
}

- (void)setupUI {
    // TODO: finish this
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Default username
    NSString *userAccount = [AppSetting getAppSetting:USER_ACCOUNT];
    if (userAccount) {
        [username setText:userAccount];
    }
    
    // Show loading progress (Login successfully before)
    if ([[AppSetting getAppSetting:USER_CERTIFIED] boolValue]) {
        [self setUIEnabled:NO];

        // Auto login
        [username setText:[AppSetting getAppSetting:USER_ACCOUNT]];
        [password setText:[AppSetting getAppSetting:USER_PASSWORD]];
        [self loginProcess];
    }
    else {
        // Reset View
        [self setUIEnabled:YES];
        [password setText:@""];
        
        // Special case
        if (isGoogleSignIn) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [spinner setHidden:NO];
            });
        }
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark Rotation

- (BOOL)shouldAutorotate {
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark Textfield UI related

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (username == textField) {
        [password becomeFirstResponder];
    }
    else if (password == textField) {
        [self login:nil];
    }

    return YES;
}

- (void)dismissKeyboard {
    [username resignFirstResponder];
    [password resignFirstResponder];
}

-(void)setUIEnabled:(BOOL)enabled {
    dispatch_async(dispatch_get_main_queue(), ^{
        loadingView.hidden = spinner.hidden = loginBtn.enabled = googleLoginBtn.enabled = forgetBtn.enabled = createBtn.enabled = username.enabled = password.enabled = enabled;
        
        [self.navigationController setNavigationBarHidden:!enabled];
    });
}

#pragma mark Login process

- (IBAction)login:(id)sender {
    [self dismissKeyboard];

    // Check input
    if ([username.text isEqualToString:@""]) {
        [Utility makeWarningToastWithTitle:NSLocalizedString(@"Login Error", nil) andMessage:NSLocalizedString(@"Please enter a valid username", nil)];
        return;
    }
    if ([password.text isEqualToString:@""]) {
        [Utility makeWarningToastWithTitle:NSLocalizedString(@"Login Error", nil) andMessage:NSLocalizedString(@"Please enter a valid password", nil)];
        return;
    }
    
    [self setUIEnabled:NO];
    [self loginProcess];
}

- (void)loginProcess {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // TODO: Check connectivity
        
        // TODO: Guest login
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [statusLbl setText:NSLocalizedString(@"Service initializing", nil)];
        });
        
        // Check if Qlync is alive
        BOOL ret = [SATWrapper loginwithUsername:username.text andPassword:password.text];
        if (NO == ret) {
            // TODO: LAN login and mark Qlync fail, login Tim server?
            
            NSLog(@"Login fail!!!!!");
            
            // Restore UI for now
            [self setUIEnabled:YES];
            
            return;
        }
        
        
        [AppSetting updateAppSetting:USER_GUEST_LOGIN withNewObject:@0];
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [statusLbl setText:NSLocalizedString(@"Retriving device list", nil)];
        });
        
        // Login fail
        if (nil == [SATWrapper getCameraList]) {
            [Utility makeWarningToastWithTitle:NSLocalizedString(@"Login Error", nil) andMessage:NSLocalizedString(@"Wrong user name or password", nil)];
            [AppSetting updateAppSetting:USER_CERTIFIED withNewObject:@0];
            
            // Restore UI
            [self setUIEnabled:YES];
        }
        // Login success
        else {
            // TODO: Add account info to Gynoii server
            
            // TODO: Add device(iPhone UDID / audio mode) info too
            
            // TODO: Add camera to Gynoii server
            
            // TODO: Check for cloud setting with Gynoii server
            
            // Login success, write credential to App settings
            [AppSetting updateAppSetting:USER_ACCOUNT withNewObject:username.text];
            [AppSetting updateAppSetting:USER_PASSWORD withNewObject:password.text];
            [AppSetting updateAppSetting:USER_CERTIFIED withNewObject:@1];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [statusLbl setText:NSLocalizedString(@"Loading...", nil)];
            });
            
            // Dismiss landing view after 1.5 secs
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self performSegueWithIdentifier:@"loginSuccessSegue" sender:nil];
            });
            
            // LAN search
            [CameraManager broadcastLAN];
            
            // Create p2p tunnels
            [SATWrapper connectAllCameras];
        }
    });
}

#pragma mark Google login
- (IBAction)googleSignIn:(id)sender {
    GIDSignIn *signIn = [GIDSignIn sharedInstance];
    [signIn signIn];
    isGoogleSignIn = YES;
}

- (void)googleLoginProcess:(NSNotification *)object {
    NSDictionary *userInfo = object.userInfo;
    username.text = [userInfo objectForKey:@"username"];
    password.text = [userInfo objectForKey:@"pwd"];
    isGoogleSignIn = NO;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self setUIEnabled:NO];
        [self loginProcess];
    });
}

- (void)errorGoogleLogin:(NSNotification*)sender {
    [self setUIEnabled:YES];
    isGoogleSignIn = NO;
    [AppSetting updateAppSetting:USER_CERTIFIED withNewObject:@0];
    [AppSetting updateAppSetting:USER_GOOGLE_LOGIN withNewObject:@0];
}

@end
