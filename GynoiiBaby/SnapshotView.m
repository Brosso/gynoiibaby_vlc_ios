//
//  SnapshotView.m
//  Babycam
//
//  Created by BingHuan Wu on 4/24/17.
//  Copyright © 2017 Gynoii. All rights reserved.
//

#import "SnapshotView.h"
#import <AVFoundation/AVFoundation.h>

@implementation SnapshotView {
    // PIP view
    UIImageView *snapshot;
    NSString *snapshotURL;
    NSTimer *updateTimer;
    UIView *parentView;
    UIActivityIndicatorView *spinner;
    
    // Time lapse
    NSTimer *timeLapseTimer;
    NSString *timeLapseURL;
    NSString *timeLapseFolderPath;
    __block int timelapseIndex;
    
    // Variables
    __block BOOL isLoading; // Prevent mutiple loading request at one time
    BOOL isTimelapsing;     // Is performing time lapse function
    BOOL isEncoding;        // Is encoding video after time lapse stop
    float encodedIndex;     // Use for percentage progress
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id)init {
    if (self = [super init]) {
        // Image view
        snapshot = [[UIImageView alloc] initWithFrame:CGRectZero];
        [snapshot setContentMode:UIViewContentModeScaleAspectFill];
        [self addSubview:snapshot];
        
        // Image frame border
        [snapshot.layer setBorderColor: [GYNOII_GREEN CGColor]];
        [snapshot.layer setBorderWidth: 2.0];
        
        // Spinner
        spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        spinner.transform = CGAffineTransformMakeScale(1.5, 1.5);
        [spinner setColor:GYNOII_GREEN];
        [spinner startAnimating];
        [spinner setHidesWhenStopped:YES];
        [self addSubview:spinner];
        
        // Gesture
        UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
        [self addGestureRecognizer:pan];
        
        // Var
        isLoading = NO;
        isTimelapsing = NO;
        isEncoding = NO;
        
        NSArray *documentsPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [documentsPaths objectAtIndex:0];
        timeLapseFolderPath = [documentsDirectory stringByAppendingPathComponent:@"TimeLapse"];
        
    }
    return self;
}

- (void)startUpdate {
    NSNumber *pipIndex = [AppSetting getAppSetting:PIP_INDEX];
    int index = [pipIndex intValue];
    if (!pipIndex || -1 == index) {
        return;
    }
    
    NSMutableArray *cameraList = [CameraManager getCameraList];
    if (index < [cameraList count]) {
        NSDictionary *dict = [cameraList objectAtIndex:index];
        BOOL lan = [[dict objectForKey:LAN_VALID] boolValue];
        int wanPort = [[dict objectForKey:WAN_PORT] intValue];
        
        if (!lan && 0 == wanPort) {
            return;
        }
        
#ifdef FORCE_P2P
        lan = NO;
        if (0 == wanPort) {
            return;
        }
#endif
        
        NSString *snapshotPath = [NSString stringWithFormat:@"http://%@:%d%@",
                                  lan?[dict objectForKey:LAN_IP]: LOCAL_HOST_IP,
                                  lan?80:wanPort,
                                  [dict objectForKey:SNAPSHOT_PATH]];
        snapshotURL = snapshotPath;
    }
    
    // Self update timer
    if (!updateTimer) {
        dispatch_async(dispatch_get_main_queue(), ^{
            updateTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateSnapshot) userInfo:nil repeats:YES];
        });
    }
}

- (void)stopUpdate {
    if (updateTimer) {
        [updateTimer invalidate];
        updateTimer = nil;
    }
}

- (void)setFrame:(CGRect)frame {
    [snapshot setFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
    spinner.center = CGPointMake(frame.size.width/2, frame.size.height/2);
    [super setFrame:frame];
}

- (void)setParentView:(UIView*)parent {
    if (parent) {
        parentView = parent;
    }
}

- (void)setHidden:(BOOL)hidden {
    if (hidden) {
        [self stopUpdate];
    }
    else {
        [self startUpdate];
    }
    
    [super setHidden:hidden];
}

- (void)updateSnapshot {
    if (!snapshotURL || [snapshotURL isEqualToString:@""]) {
        // Invalid path
        return;
    }
    
    if (isLoading) {
        return;
    }

    isLoading = YES;
    
    void (^callBack)(NSURLResponse* response, NSData* data, NSError* connectionError) = ^(NSURLResponse* response, NSData* data, NSError* error) {
        @autoreleasepool {
            
            isLoading = NO;
            
            if([data length] > 0 && error == nil){
                UIImage *image = [UIImage imageWithData:data];
                if (image) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [snapshot setImage:image];
                        if ([spinner isAnimating]) {
                            [spinner stopAnimating];
                        }
                    });
                }
            }
            
            // No return - error handle
            else {
//                if (arc4random()%10 < 1) { // 10%
//                    [[NSNotificationCenter defaultCenter] postNotificationName:SAT_WAN_ERR object:nil];
//                }
                
                // Remove last image view
                dispatch_async(dispatch_get_main_queue(), ^{
                    [spinner startAnimating];
                    [snapshot setImage:nil];
                });
                
                [self stopUpdate];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self startUpdate];
                });
                
            }
        }
    };
    
    [Utility makeURLRequest:snapshotURL withCallback:callBack];
}

- (void)handlePan:(UIPanGestureRecognizer*)recognizer {
    if (parentView) {
        CGPoint translation = [recognizer translationInView:parentView];
        self.center = CGPointMake(self.center.x + translation.x,
                                  self.center.y + translation.y);
        [recognizer setTranslation:CGPointMake(0, 0) inView:parentView];
    }
}

# pragma mark Timelapse function
- (BOOL)timelapseDidTouch:(NSInteger)index {
    if (isEncoding) {
        [Utility makeWarningToastWithTitle:NSLocalizedString(@"Warning", nil) andMessage:NSLocalizedString(@"Still processing previous video, please wait", nil)];
        return NO;
    }
    
    // Self toggle
    isTimelapsing = !isTimelapsing;
    
    if (isTimelapsing) {
        NSMutableArray *cameraList = [CameraManager getCameraList];
        if (index < [cameraList count]) {
            NSDictionary *dict = [cameraList objectAtIndex:index];
            BOOL lan = [[dict objectForKey:LAN_VALID] boolValue];
            int wanPort = [[dict objectForKey:WAN_PORT] intValue];
            
            if (!lan && 0 == wanPort) {
                return NO;
            }
            
#ifdef FORCE_P2P
            lan = NO;
            if (0 == wanPort) {
                return NO;
            }
#endif
            
            NSString *timeLapsePath = [NSString stringWithFormat:@"http://%@:%d%@",
                                      lan?[dict objectForKey:LAN_IP]: LOCAL_HOST_IP,
                                      lan?80:wanPort,
                                      [dict objectForKey:SNAPSHOT_PATH]];
            timeLapseURL = timeLapsePath;
            [self startTimeLapse];
        }
    }
    else {
        [self stopTimeLapse];
    }
    
    return isTimelapsing;
}

- (void)startTimeLapse {
    // Remove old contents
    [[NSFileManager defaultManager] removeItemAtPath:timeLapseFolderPath error:nil];
    
    // Create new folder
    BOOL directory;
    if (![[NSFileManager defaultManager] fileExistsAtPath:timeLapseFolderPath isDirectory: &directory]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:timeLapseFolderPath withIntermediateDirectories:NO attributes:nil error:nil];
    }
    
    // Init timelapseIndex var
    timelapseIndex = -1;
    
    // Start time lapse timer
    if (!timeLapseTimer) {
        dispatch_async(dispatch_get_main_queue(), ^{
            timeLapseTimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(timeLapseProcess) userInfo:nil repeats:YES];
            
            // Local notification
            UILocalNotification *localNotification = [[UILocalNotification alloc] init];
            localNotification.alertBody = NSLocalizedString(@"Start TimeLapse", nil);
            localNotification.soundName = UILocalNotificationDefaultSoundName;
            [[UIApplication sharedApplication] cancelLocalNotification:localNotification];
            [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            
            // Set mode in user defaults
            [AppSetting updateAppSetting:TIMELAPSE_MODE withNewObject:@1];
        });
    }
}

- (void)timeLapseProcess {
    timelapseIndex++;
    
    if (timelapseIndex < TIMELAPSE_GAP1) {
        
    } else if (timelapseIndex < TIMELAPSE_GAP2) {
        if (timelapseIndex % 2 != 0) {
            return;
        }
    } else if (timelapseIndex < TIMELAPSE_GAP3) {
        if (timelapseIndex % 4 != 0) {
            return;
        }
    } else if (timelapseIndex < TIMELAPSE_GAP4) {
        if (timelapseIndex % 8 != 0) {
            return;
        }
    } else if (timelapseIndex < TIMELAPSE_GAP5) {
        if (timelapseIndex % 16 != 0) {
            return;
        }
    } else {
        [self stopTimeLapse];
        return;
    }
    
    NSString *savedFileName = [NSString stringWithFormat:@"%05d.png",timelapseIndex];
    NSString *savedImagePath = [timeLapseFolderPath stringByAppendingPathComponent:savedFileName];
    
    void (^callBack)(NSURLResponse* response, NSData* data, NSError* connectionError) = ^(NSURLResponse* response, NSData* data, NSError* error) {
        @autoreleasepool {
            if([data length] > 0 && error == nil) {
                [data writeToFile:savedImagePath atomically:YES];
//                NSLog(@"write image to %@", savedImagePath);
            } else {
                // Error, cannot get image
                NSLog(@"Error writing %@", savedImagePath);
                timelapseIndex --;
                return ;
            }
        }
    };
    
    [Utility makeURLRequest:timeLapseURL withCallback:callBack];
}

- (void)stopTimeLapse {
    if (timeLapseTimer) {
        [timeLapseTimer invalidate];
        timeLapseTimer = nil;
        
        // Encode video in background thread
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [self encodeVideo];
        });
        
        // Set mode in user defaults
        [AppSetting updateAppSetting:TIMELAPSE_MODE withNewObject:@0];
    }
}

- (void)encodeVideo {
    NSArray *folderContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:timeLapseFolderPath error:nil];
    if (nil == folderContent || 0 == [folderContent count]) {
        NSLog(@"nil folder");
        return;
    }
    
    NSString *firstImagePath = [timeLapseFolderPath stringByAppendingPathComponent:[folderContent firstObject]];
    UIImage *firstFrame = [UIImage imageWithContentsOfFile:firstImagePath];
    
    if (nil == firstFrame) {
        NSLog(@"nil image");
        return;
    }
    
    // Start encode process
    isEncoding = YES;
    CGSize frameSize = firstFrame.size;
    
    // Init var
    NSString *outputPath = [NSString stringWithFormat:@"%@/%@", [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0], @"timelapse.mp4"];
    NSURL* outputURL = [NSURL fileURLWithPath:outputPath];
    
    // Remove old video
    NSFileManager* fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:outputPath]) {
        [fileManager removeItemAtPath:outputPath error:nil];
    }
    
    // Setup writer
    NSError *err = nil;
    AVAssetWriter *assetWriter = [[AVAssetWriter alloc] initWithURL:outputURL fileType:AVFileTypeMPEG4 error:&err];
   
    // Mark metadata as TIMELAPSE
    AVMutableMetadataItem *mutableItem = [AVMutableMetadataItem metadataItem];
    mutableItem.key = AVMetadataCommonKeyTitle;
    mutableItem.keySpace = AVMetadataKeySpaceCommon;
    mutableItem.locale = [NSLocale currentLocale];
    mutableItem.value = @"TIMELAPSE";

    assetWriter.metadata = [NSArray arrayWithObject:mutableItem];
    NSParameterAssert(assetWriter);
    
    // Input setting
    NSDictionary *videoSettings = [NSDictionary dictionaryWithObjectsAndKeys:
                                   AVVideoCodecH264, AVVideoCodecKey,
                                   [NSNumber numberWithInt:frameSize.width], AVVideoWidthKey,
                                   [NSNumber numberWithInt:frameSize.height], AVVideoHeightKey,
                                   nil];
    AVAssetWriterInput *videoWriterInput = [[AVAssetWriterInput alloc] initWithMediaType:AVMediaTypeVideo outputSettings:videoSettings];
    NSParameterAssert(videoWriterInput);
    videoWriterInput.expectsMediaDataInRealTime = YES;
    
    // Setup Adaptor
    NSDictionary* bufferAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                      [NSNumber numberWithInt:kCVPixelFormatType_32ARGB],kCVPixelBufferPixelFormatTypeKey, [NSNumber numberWithInt:frameSize.width], kCVPixelBufferWidthKey, [NSNumber numberWithInt:frameSize.height], kCVPixelBufferHeightKey, nil];
    
    AVAssetWriterInputPixelBufferAdaptor *avAdaptor = [[AVAssetWriterInputPixelBufferAdaptor alloc] initWithAssetWriterInput:videoWriterInput sourcePixelBufferAttributes:bufferAttributes];
    
    // Add input and start session
    [assetWriter addInput:videoWriterInput];
    [assetWriter startWriting];
    [assetWriter startSessionAtSourceTime:kCMTimeZero];
    int64_t timestamp = 0.0f;
    
    int gap = 1;
    if (timelapseIndex < TIMELAPSE_GAP1) {
        gap = 1;
    } else if (timelapseIndex < TIMELAPSE_GAP2) {
        gap = 2;
    } else if (timelapseIndex < TIMELAPSE_GAP3) {
        gap = 4;
    } else if (timelapseIndex < TIMELAPSE_GAP4) {
        gap = 8;
    } else {
        gap = 16;
    }
    
    encodedIndex = 0; // circular progress indicator
    // Post init message
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:ENCODE_PROGRESS
                                                            object:[NSNumber numberWithFloat:0]];
    });

    // Append image to pixel buffer
    for (NSString *pathComponent in folderContent) {
        int index = [[pathComponent substringToIndex:5] intValue];
        if(index % gap != 0){
            continue;
        }
        
        // Post update message
        dispatch_async(dispatch_get_main_queue(), ^{
            float i = encodedIndex / [folderContent count];
            [[NSNotificationCenter defaultCenter] postNotificationName:ENCODE_PROGRESS object:[NSNumber numberWithFloat:i]];
        });
        encodedIndex += 1.0f;
        
        NSString *path = [timeLapseFolderPath stringByAppendingPathComponent:pathComponent];
        if (![videoWriterInput isReadyForMoreMediaData]) {
            NSLog(@"videoWriterInput not ready for video data");
        }
        else {
            @autoreleasepool {
                UIImage* newFrame = [UIImage imageWithContentsOfFile:path];
                // nil check - will cause encode crash
                if (nil == newFrame) {
                    NSLog(@"Skip nil image");
                    continue;
                }
#ifdef WATERMARK
                UILabel *newLabel = [[UILabel alloc] initWithFrame:CGRectZero];
                newLabel.backgroundColor = [UIColor clearColor];
                newLabel.font = [UIFont boldSystemFontOfSize:20.0];
                newLabel.textAlignment = NSTextAlignmentCenter;
                newLabel.textColor = [UIColor whiteColor];
                newLabel.text = @"安安你好";
                newLabel.backgroundColor = [UIColor clearColor];
                [newLabel sizeToFit];
                
                UIImage *watermark = [UIImage imageNamed:@"snapshot_active.png"];
                UIGraphicsBeginImageContextWithOptions(newFrame.size, FALSE, 1.0);
                [newFrame drawInRect:CGRectMake(0, 0, newFrame.size.width, newFrame.size.height)];
                [watermark drawInRect:CGRectMake(20, 50, watermark.size.width, watermark.size.height)];
                [newLabel drawTextInRect:CGRectMake(20, 100, newLabel.frame.size.width, newLabel.frame.size.height)];
                UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
                newFrame = newImage;
#endif
                CVPixelBufferRef pixelBuffer = NULL;
                CGImageRef cgImage = CGImageCreateCopy([newFrame CGImage]);
                CFDataRef image = CGDataProviderCopyData(CGImageGetDataProvider(cgImage));
                
                // Create pixel buffer
                NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                                         [NSNumber numberWithBool:YES], kCVPixelBufferCGImageCompatibilityKey,
                                         [NSNumber numberWithBool:YES], kCVPixelBufferCGBitmapContextCompatibilityKey,
                                         nil];
                
                CVPixelBufferCreate(kCFAllocatorDefault, frameSize.width,
                                    frameSize.height, kCVPixelFormatType_32ARGB, (__bridge CFDictionaryRef) options,
                                    &pixelBuffer);
                
                CVPixelBufferLockBaseAddress(pixelBuffer, 0);
                void *pxdata = CVPixelBufferGetBaseAddress(pixelBuffer);
                
                // Adjust image format
                CGColorSpaceRef rgbColorSpace = CGColorSpaceCreateDeviceRGB();
                CGContextRef context = CGBitmapContextCreate(pxdata, frameSize.width,
                                                             frameSize.height, 8, 4*frameSize.width, rgbColorSpace,
                                                             kCGImageAlphaNoneSkipFirst);
                
                CGContextConcatCTM(context, CGAffineTransformMakeRotation(0));
                CGContextDrawImage(context, CGRectMake(0, 0, CGImageGetWidth(cgImage),
                                                       CGImageGetHeight(cgImage)), cgImage);
                CGColorSpaceRelease(rgbColorSpace);
                CGContextRelease(context);
                
                //CVPixelBufferUnlockBaseAddress(pixelBuffer, 0);
                
                // Append image to pixel buffer
                BOOL success = [avAdaptor appendPixelBuffer:pixelBuffer withPresentationTime:CMTimeMake(timestamp, 1000)];
                timestamp += 30.0f;
                if (!success) {
                    NSLog(@"Warning: Unable to write buffer to video");
                }
                
                // Clean up
                CVPixelBufferUnlockBaseAddress( pixelBuffer, 0 );
                CVPixelBufferRelease( pixelBuffer );
                CFRelease(image);
                CGImageRelease(cgImage);
                
                newFrame = nil;
                
#ifdef WATERMARK
                newLabel = nil;
                watermark = nil;
                newImage = nil;
#endif
            }
        }
        
    }
    
    NSLog(@"Complete writing");
    
    // Complete writing
    [videoWriterInput markAsFinished];
    
    // Wait for the asset writer
    AVAssetWriterStatus status = assetWriter.status;
    while (AVAssetWriterStatusUnknown == status) {
        [NSThread sleepForTimeInterval:0.5f]; // Bad practice - busy waiting
        status = assetWriter.status;
    }
    
    @synchronized(self) {
        // Save video to album
        [assetWriter finishWritingWithCompletionHandler:^{
            [Utility saveVideoURL:outputURL toAlbum:ALBUM_TITLE];
            
            // Post init message
            dispatch_async(dispatch_get_main_queue(), ^{
                [[NSNotificationCenter defaultCenter] postNotificationName:ENCODE_PROGRESS
                                                                    object:[NSNumber numberWithFloat:1.1f]];
            });
        }];
    }
    
    // Delete temp snapshot folder
    [[NSFileManager defaultManager] removeItemAtPath:timeLapseFolderPath error:nil];
    
    
    isEncoding = NO;
}

@end
