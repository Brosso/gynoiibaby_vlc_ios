//
//  AppSetting.m
//  GynoiiBaby
//
//  Created by BingHuan Wu on 3/14/17.
//  Copyright © 2017 Gynoii. All rights reserved.
//

#import "AppSetting.h"

@implementation AppSetting

+ (void) initializeAppSetting {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *appSettings = [[defaults objectForKey:APP_SETTINGS] mutableCopy];
    
    // First install TODO: Merge from old app
    if (!appSettings) {
        NSLog(@"Brosso: first install");
        appSettings = [NSMutableDictionary dictionary];
        [appSettings setObject:[NSDate date] forKey:APP_INSTALL_DATE];
        [appSettings setObject:@"" forKey:USER_ACCOUNT];
        [appSettings setObject:@"" forKey:USER_PASSWORD];
        [appSettings setObject:@"" forKey:USER_EMAIL];
        [appSettings setObject:@"" forKey:USER_LOCATION];
        [appSettings setObject:@"" forKey:USER_COUNTRY];
        [appSettings setObject:@0  forKey:USER_CERTIFIED];
        [appSettings setObject:@0  forKey:USER_GOOGLE_LOGIN];
        [appSettings setObject:@0  forKey:USER_GUEST_AUDIO];
        [appSettings setObject:@0  forKey:USER_GUEST_LOGIN];
        [appSettings setObject:@0  forKey:USER_GUEST_ENABLED];
        [appSettings setObject:@-1  forKey:PIP_INDEX];
        // TODO: merge with old

        [defaults setObject:appSettings forKey:APP_SETTINGS];
        [defaults synchronize];
    }
    else {
        // Reset all camera valid field
        [CameraManager resetCameraValidation];
    }
}

+ (id)getAppSetting:(NSString *)key {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *appSettings = [defaults objectForKey:APP_SETTINGS];
    return [appSettings objectForKey:key];
}

+ (void)updateAppSetting:(NSString *)key withNewObject:(id)object {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *appSettings = [[defaults objectForKey:APP_SETTINGS] mutableCopy];
    
    [appSettings setObject:object forKey:key];
    [defaults setObject:appSettings forKey:APP_SETTINGS];
    [defaults synchronize];
}

@end
