//
//  AppDelegate.h
//  GynoiiBaby
//
//  Created by BingHuan Wu on 3/7/17.
//  Copyright © 2017 Gynoii. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Google/SignIn.h>
#import "SATWrapper.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate, GIDSignInDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

