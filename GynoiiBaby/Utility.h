//
//  Utility.h
//  GynoiiBaby
//
//  Created by BingHuan Wu on 3/14/17.
//  Copyright © 2017 Gynoii. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonDigest.h>
#import "UIView+Toast.h"

// UI related define
#define GYNOII_GREEN ([UIColor colorWithRed:122.0f/255 green:204.0f/255 blue:153.0f/255 alpha:1.0f])
#define GYNOII_BLUE ([UIColor colorWithRed:102.0f/255 green:157.0f/255 blue:194.0f/255 alpha:1.0f])
#define GYNOII_DARKGREEN ([UIColor colorWithRed:71.0f/255 green:173.0f/255 blue:133.0f/255 alpha:1.0f])

// Gynoii Server related define
#define GYNOII_SECRET @"2365827800000000"
#define SERVER_URL @"https://api.gynoii.com"

@interface Utility : NSObject

// Toast (with different icon and duration)
+ (void)makeInfoToastWithTitle: (NSString*)title andMessage: (NSString*)message;
+ (void)makeWarningToastWithTitle: (NSString*)title andMessage: (NSString*)message;

// Deprecated alert
+ (void)showAlertWithTitle: (NSString*)title andMessage: (NSString*)message;

// HTTP GET and POST
+(void)makeURLRequest:(NSString*)URLString withCallback:(void (^)(NSURLResponse* response, NSData* data, NSError* connectionError))callback;
+(void)makeURLPostRequest:(NSString*)URLString withParams:(NSDictionary*)param andCallback:(void (^)(NSURLResponse* response, NSData* data, NSError* connectionError))callback;

// Format check
+(BOOL)isValidEmail:(NSString *)checkString;
+(BOOL)isValidNameFormat:(NSString *)checkString;  // Camera name
+(BOOL)isValidQlyncFormat:(NSString *)checkString; // User account name
+(NSString*)MD5:(NSString*)input;

// Gynoii service
+ (void)logEvent:(NSString*)event;
+ (void)uploadEvents;

// Album
+ (void)saveVideoURL:(NSURL*)fileURL toAlbum:(NSString*)albumTitle;

// Microphone
+ (void)checkMicrophonePermission:(void (^)(BOOL granted))callback;

@end
