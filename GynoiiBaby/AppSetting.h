//
//  AppSetting.h
//  GynoiiBaby
//
//  Created by BingHuan Wu on 3/14/17.
//  Copyright © 2017 Gynoii. All rights reserved.
//

#import <Foundation/Foundation.h>

#define APP_SETTINGS        @"kAPP_SETTINGS"        // (NSString) App setting identifier
#define APP_INSTALL_DATE    @"kAPP_INSTALL_DATE"    // (NSDate) Install date

#define USER_ACCOUNT        @"kUSER_ACCOUNT"        // (NSString) User name
#define USER_PASSWORD       @"kUSER_PASSWORD"       // (NSString) User password
#define USER_EMAIL          @"kUSER_EMAIL"          // (NSString) User email
#define USER_CERTIFIED      @"kUSER_CERTIFIED"      // (NSNumber) If user login successful
#define USER_LOCATION       @"kUSER_LOCATION"       // (NSString) User location ?
#define USER_COUNTRY        @"kUSER_COUNTRY"        // (NSString) User country
#define USER_GOOGLE_LOGIN   @"kUSER_GOOGLE_LOGIN"   // (NSNumber) If using Google login
#define USER_GUEST_LOGIN    @"kUSER_GUEST_LOGIN"    // (NSNumber) If using guest login
#define USER_GUEST_AUDIO    @"kUSER_GUEST_AUDIO"    // (NSNumber) If guest audio enabled
#define USER_GUEST_ENABLED  @"kUSER_GUEST_ENABLED"  // (NSNumber) If guest login enabled

#define PIP_INDEX           @"kPIP_INDEX"           // (NSNumber) Index to load for PIP view (-1 for none)

#define GOOGLE_LOGIN_SUCCESS    @"kGOOGLE_LOGIN_SUCCESS"    // Notification name
#define GOOGLE_LOGIN_FAIL       @"kGOOGLE_LOGIN_FAIL"       // Notification name
#define ENCODE_PROGRESS         @"kENCODE_PROGRESS"         // Notification name


#define ALBUM_TITLE         (NSLocalizedString(@"Gynoii Baby Monitor", nil))


// -------------------------------------------------------------------------------------------
#define AUDIO_MODE          @"kAUDIO_MODE"          // (NSNumber) If audio background mode enabled
#define TIMELAPSE_MODE      @"kTIMELAPSE_MODE"      // (NSNumber) If in timelapse mode
#define UPDATE_CHECK        @"kUPDATE_CHECK"        // app version check
#define BLOG_VER            @"kBLOG_VER"            // blog version check
#define BLOG_NEW            @"kBLOG_NEW"            // new update in blog
#define DISPLAY_INFO        @"kDISPLAY_INFO"        // display baby's info

#define SETTING_LIST        @"SETTING_LIST"         // array of dictionaries contains cloud settings
#define MOTION_DETECT       @"MOTION_DETECT"        // motion detection: 0:off / 1:low / 2:med / 3:high
#define AUDIO_DETECT        @"AUDIO_DETECT"         // audio detection: 0:off / 1:low / 2:med / 3:high
#define LED                 @"LED"                  // 0:off / 1:on
#define LULLABY_MODE        @"LULLABY_MODE"         // 0:off / 1:on
#define LULLABY_SONG        @"LULLABY_SONG"         // lullaby music name
#define QUALITY             @"QUALITY"              // video quality
#define NIGHT_VISION        @"NIGHT_VISION"         // 0:auto / 1:on / 2:off
#define FIRMWARE_VER        @"FIRMWARE_VER"         // firmware version
#define VOLUME              @"VOLUME"               // volume

#define UNBIND_MAC      @"UNBIND_MAC"
#define UNBIND_ACT      @"UNBIND_ACT"

#define EVENT_LIST              @"EVENT_LIST" // dictionary with button event (key) and counts number (value)
#define CLICK_TIMELAPSE         @"TIMELAPSE"
#define CLICK_TALK              @"TALK"
#define CLICK_MUSIC             @"MUSIC"
#define CLICK_RECORD            @"RECORD"
#define CLICK_MUTE              @"MUTE"
#define CLICK_PTZ               @"PANTILT"
#define CLICK_SNAPSHOT          @"SNAPSHOT"
#define CLICK_SHARE             @"SHARE"
#define ACTIVE_MAIN             @"ACTIVE_MAIN" // main login
#define ACTIVE_GUEST            @"ACTIVE_GUEST"// guest login
#define CLICK_EDITOR_START      @"EDITOR_START"
#define CLICK_EDITOR_ROLL       @"EDITOR_ROLL"
#define CLICK_EDITOR_FINISH     @"EDITOR_FINISH"
#define VIEW_LOCAL              @"LOCAL"
#define VIEW_REMOTE             @"REMOTE"
#define QLYNC_OFFLINE           @"QLYNC_OFFLINE"
#define QLYNC_FAIL              @"QLYNC_FAIL"

@interface AppSetting : NSObject

+ (void) initializeAppSetting;
+ (id)getAppSetting:(NSString *)key;
+ (void)updateAppSetting:(NSString *)key withNewObject:(id)object;

@end
