//
//  SettingViewController.h
//  GynoiiBaby
//
//  Created by BingHuan Wu on 3/8/17.
//  Copyright © 2017 Gynoii. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingViewController : UIViewController <UITableViewDelegate, UITableViewDataSource> {
    IBOutlet UIBarButtonItem *backButton;
    IBOutlet UITableView *settingTableView;
}

@end
