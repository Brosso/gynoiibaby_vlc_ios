//
//  LiveViewController.h
//  GynoiiBaby
//
//  Created by BingHuan Wu on 3/7/17.
//  Copyright © 2017 Gynoii. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MPMusicPlayerController.h>
#import "HomeMadeLiveViewController.h"
#import "SnapshotView.h"

@interface LiveViewController : UIViewController <UIScrollViewDelegate> {
    HomeMadeLiveViewController  *gynoiiPlayer;
    MPMusicPlayerController     *volumeControl;
    UIView                      *addCameraView;
    SnapshotView                *pipView;
    
    IBOutlet UIScrollView       *scrollView;
    IBOutlet UINavigationItem   *customNavigationItem;
    IBOutlet UIPageControl      *pageControl;
    IBOutlet UINavigationBar    *customNavBar;
    
    IBOutlet UIButton           *snapShotBtn;
    IBOutlet UIButton           *talkBtn;
    
    IBOutlet UIView             *menuBar;
    IBOutlet UIButton           *musicBtn;
    IBOutlet UIButton           *recordBtn;
    IBOutlet UIButton           *timelapseBtn;
    IBOutlet UIButton           *muteBtn;
    IBOutlet UIButton           *ptzBtn;
    
    IBOutlet NSLayoutConstraint *encodeIndicatorTop;
}

@end
