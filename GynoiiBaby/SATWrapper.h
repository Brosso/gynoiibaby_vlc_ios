//
//  SATWrapper.h
//  GynoiiBaby
//
//  Created by BingHuan Wu on 3/9/17.
//  Copyright © 2017 Gynoii. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SatManager.h"

@interface SATWrapper : NSObject
@property (strong, nonatomic) SatManager *manager;

+ (id)sharedInstance;
+ (SatManager*)sharedManager;
+ (BOOL)loginwithUsername:(NSString*)username andPassword:(NSString*)password;
+ (NSMutableArray *)getCameraList;
+ (NSDictionary *)startP2PWithUID:(NSString*)uid andPort:(NSUInteger)port;

+ (void)connectAllCameras;
+ (void)connectAllCamerasOnPort:(int)port;
+ (void)connectCamera:(NSString*)uid;
+ (void)connectCamera:(NSString*)uid onPort:(int)port;
+ (void)executeJobQueue:(NSMutableArray *)jobQueue withPort:(int)port;

@end
