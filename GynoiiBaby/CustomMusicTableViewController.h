//
//  CustomMusicTableViewController.h
//  GynoiiBaby
//
//  Created by BingHuan Wu on 25/05/2017.
//  Copyright © 2017 Gynoii. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>

@interface CustomMusicTableViewController : UITableViewController <MPMediaPickerControllerDelegate>

@end
