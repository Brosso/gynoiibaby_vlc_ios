//
//  CreateAccountViewController.h
//  GynoiiBaby
//
//  Created by BingHuan Wu on 3/15/17.
//  Copyright © 2017 Gynoii. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreateAccountViewController : UIViewController <UITextFieldDelegate> {
    IBOutlet UITextField *username;
    IBOutlet UITextField *password;
    IBOutlet UITextField *confirmPassword;
    IBOutlet UITextField *email;
    IBOutlet UIScrollView *scrollView;
    IBOutlet UIActivityIndicatorView *spinner;
}

@end
