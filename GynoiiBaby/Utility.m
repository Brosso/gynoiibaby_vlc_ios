//
//  Utility.m
//  GynoiiBaby
//
//  Created by BingHuan Wu on 3/14/17.
//  Copyright © 2017 Gynoii. All rights reserved.
//

#import "Utility.h"

@import Photos;
@implementation Utility

#pragma mark UI (toast and alert)
+(void)makeInfoToastWithTitle:(NSString*)title andMessage:(NSString*)message {
    dispatch_async(dispatch_get_main_queue(), ^{
        UIView* topView = (UIView*)[[[UIApplication sharedApplication] windows] firstObject];
        CGRect frame = topView.frame;
        if (![title isEqualToString:@""]) {
            [topView makeToast:message duration:1.5 position:[NSValue valueWithCGPoint:CGPointMake(frame.size.width/2, frame.size.height*5/6)] title:title image:[UIImage imageNamed:@"ic_versioninfo"]];
        }
        else {
            [topView makeToast:message duration:1.5 position:[NSValue valueWithCGPoint:CGPointMake(frame.size.width/2, frame.size.height*5/6)] image:[UIImage imageNamed:@"ic_versioninfo"]];
        }
    });
}

+(void)makeWarningToastWithTitle:(NSString*)title andMessage:(NSString*)message {
    dispatch_async(dispatch_get_main_queue(), ^{
        UIView* topView = (UIView*)[[[UIApplication sharedApplication] windows] firstObject];
        CGRect frame = topView.frame;
        if (![title isEqualToString:@""]) {
            [topView makeToast:message duration:3.0 position:[NSValue valueWithCGPoint:CGPointMake(frame.size.width/2, frame.size.height*5/6)] title:title image:[UIImage imageNamed:@"ic_notification"]];
        }
        else {
            [topView makeToast:message duration:3.0 position:[NSValue valueWithCGPoint:CGPointMake(frame.size.width/2, frame.size.height*5/6)] image:[UIImage imageNamed:@"ic_notification"]];
        }
    });
}

// TODO: Deprecated method
+(void)showAlertWithTitle:(NSString*)title andMessage:(NSString*)message {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
    dispatch_async(dispatch_get_main_queue(), ^{
        [alert show];
    });
}

#pragma mark URL request
// HTTP GET
+(void)makeURLRequest:(NSString*)URLString withCallback:(void (^)(NSURLResponse* response, NSData* data, NSError* connectionError))callback {
#ifdef DEBUG
//    NSLog(@"makeURLRequest( %@ )",URLString);
#endif
    
    // Cast string to standard URL
    NSURL *requestURL = [NSURL URLWithString:[URLString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSURLRequest *request = [NSURLRequest requestWithURL:requestURL];
    NSOperationQueue *queue = [NSOperationQueue currentQueue];
    
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:(callback)?callback:^(NSURLResponse *response, NSData *data, NSError *error) {
#ifdef DEBUG
        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        if (jsonDict) {
            NSLog(@"return data: %@", jsonDict);
        }
        if (error) {
            NSLog(@"error: %@",[error localizedDescription]);
        }
#endif
    }];
}

// HTTP POST method
+(void)makeURLPostRequest:(NSString*)URLString withParams:(NSDictionary*)param andCallback:(void (^)(NSURLResponse* response, NSData* data, NSError* connectionError))callback {
    
    NSURL *requestURL = [NSURL URLWithString:[URLString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
    
    NSMutableString *paramString = [NSMutableString string];
    for (NSString *key in [param allKeys]) {
        NSString *value = [param objectForKey:key];
        [paramString appendFormat:@"%@=%@&",key,value];
    }
    
    NSString *postLength = [NSString stringWithFormat:@"%d", (int)[paramString length]];
    
    request.HTTPMethod = @"POST";
    request.HTTPBody = [paramString dataUsingEncoding:NSUTF8StringEncoding];
    [request setValue:@"application/x-www-form-urlencoded; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setValue: postLength forHTTPHeaderField:@"Content-Length"];
    
    NSOperationQueue *queue = [NSOperationQueue currentQueue];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:callback];
}

#pragma mark Format check
+(BOOL)isValidEmail:(NSString *)checkString {
    // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter? stricterFilterString: laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

+(BOOL)isValidNameFormat:(NSString *)checkString {
    NSString *laxString = @"[A-Za-z0-9_-]+"; // include underscore and dash
    NSPredicate *qlyncTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", laxString];
    return [qlyncTest evaluateWithObject:checkString];
}

+(BOOL)isValidQlyncFormat:(NSString *)checkString {
    if ([checkString length] < 4 || [checkString length] > 32) {
        return NO;
    }
    NSString *laxString = @"[A-Za-z0-9]+";
    NSPredicate *qlyncTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", laxString];
    return [qlyncTest evaluateWithObject:checkString];
}

+(NSString*)MD5:(NSString*)input {
    // Create pointer to the string as UTF8
    const char *ptr = [input UTF8String];
    
    // Create byte array of unsigned chars
    unsigned char md5Buffer[CC_MD5_DIGEST_LENGTH];
    
    // Create 16 byte MD5 hash value, store in buffer
    CC_MD5(ptr, (CC_LONG)strlen(ptr), md5Buffer);
    
    // Convert MD5 value in the buffer to NSString of hex values
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x",md5Buffer[i]];
    
    return output;
}

#pragma mark Gynoii service
+ (void)logEvent:(NSString*)event {
    NSMutableDictionary *dict = [[AppSetting getAppSetting:EVENT_LIST] mutableCopy];

    if (nil == dict) {
        dict = [[NSMutableDictionary alloc] init];
        [dict setObject:@1 forKey:event];
    }
    else {
        int count = [[dict objectForKey:event] intValue];
        [dict setObject:[NSNumber numberWithInt:count+1] forKey:event];
    }

    [AppSetting updateAppSetting:EVENT_LIST withNewObject:dict];
}

+ (void)uploadEvents {
    NSMutableDictionary *dict = [AppSetting getAppSetting:EVENT_LIST];
    if (nil == dict || 0 == [[dict allKeys] count]) {
        return;
    }
    
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyyMM"];
    NSString *strDate = [dateFormatter stringFromDate:date];
    
    NSError *error = nil;
    // Pass 0 if you don't care about the readability of the generated string
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict
                                                       options:0
                                                         error:&error];
    if (jsonData) {
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSString *jsonStringEncode = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,(CFStringRef)jsonString,NULL,(CFStringRef)@"!*'();:@&=+$,/?%#[]",kCFStringEncodingUTF8));
        NSString *request = [NSString stringWithFormat:@"%@/StatService/batch.php?os=ios&month=%@&data=%@",SERVER_URL,strDate,jsonStringEncode];
        
        [Utility makeURLRequest:request withCallback:nil];
        
        // Reset EVENT_LIST
        NSMutableDictionary *newDict = [NSMutableDictionary dictionary];
        [AppSetting updateAppSetting:EVENT_LIST withNewObject:newDict];
    }
}

#pragma mark Album
+ (void)saveVideoURL:(NSURL*)fileURL toAlbum:(NSString*)albumTitle {
    PHAssetCollection *gynoiiCollection = nil;
    
    // Check if custom album existed
    PHFetchResult *userCollection = [PHAssetCollection fetchTopLevelUserCollectionsWithOptions:nil];
    for (PHAssetCollection *collection in userCollection) {
        if ([collection localizedTitle]) {
            if ([albumTitle isEqualToString:[collection localizedTitle]]) {
                gynoiiCollection = collection;
                break;
            }
        }
    }
    
    if (nil != gynoiiCollection) {
        [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
            PHAssetChangeRequest *createAssetRequest = [PHAssetChangeRequest creationRequestForAssetFromVideoAtFileURL:fileURL];
            PHAssetCollectionChangeRequest *albumChangeRequest = [PHAssetCollectionChangeRequest changeRequestForAssetCollection:gynoiiCollection];
            PHObjectPlaceholder *placeHolder = [createAssetRequest placeholderForCreatedAsset];
            [albumChangeRequest addAssets:@[placeHolder]];
            
        } completionHandler:^(BOOL success, NSError * _Nullable error) {
            if (error) {
                [Utility makeWarningToastWithTitle:NSLocalizedString(@"Error", nil) andMessage:[error localizedDescription]];
            }
            else {
                [Utility makeInfoToastWithTitle:NSLocalizedString(@"Hint", nil) andMessage:NSLocalizedString(@"Saved to gallery", nil)];
            }
        }];
    }
    else {
        NSLog(@"album not exist");
    }
}

#pragma mark Microphone
+ (void)checkMicrophonePermission:(void (^)(BOOL granted))callback {
    if ([[AVAudioSession sharedInstance] respondsToSelector:@selector(requestRecordPermission:)]) {
        [[AVAudioSession sharedInstance] requestRecordPermission:callback];
    }
}

@end
