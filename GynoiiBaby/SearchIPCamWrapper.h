//
//  SearchIPCamWrapper.h
//  Babycam
//
//  Created by BingHuan Wu on 12/31/15.
//  Copyright © 2015 Gynoii. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SearchIPCam.h"

@interface SearchIPCamWrapper : NSObject {
    SearchIPCam *search;
}

+ (id)sharedInstance;
- (NSArray*)getAllInfo;

@end
