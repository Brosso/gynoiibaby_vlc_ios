//
//  CameraManager.m
//  GynoiiBaby
//
//  Created by BingHuan Wu on 3/13/17.
//  Copyright © 2017 Gynoii. All rights reserved.
//

#import "CameraManager.h"

@implementation CameraManager

+ (id)sharedInstance {
    // structure used to test whether the block has completed or not
    static dispatch_once_t p = 0;
    
    // initialize sharedObject as nil (first call only)
    __strong static id _sharedObject = nil;
    
    // executes a block object once and only once for the lifetime of an application
    dispatch_once(&p, ^{
        _sharedObject = [[self alloc] init];
        if (_sharedObject) {
            CameraManager *castObject = ((CameraManager*) _sharedObject);
            castObject.cameraList = [NSMutableArray array];
            [castObject addObserver:castObject forKeyPath:@"cameraList" options:NSKeyValueObservingOptionNew context:nil];
        }
    });
    
    // returns the same object each time
    return _sharedObject;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    static id lastChange = nil;
    id newChange = [change objectForKey:@"new"];
    if (![lastChange isEqual:newChange] &&  newChange != [NSNull null]) {
        lastChange = newChange;
//        NSLog(@"Brosso: cameraList updated!!!");
        [[NSNotificationCenter defaultCenter] postNotificationName:LIST_UPDATED_NOTIFICATION object:nil];
    }
}

+ (void)setCamCameraList:(NSMutableArray*)newCameraList {
    if (newCameraList) {
        CameraManager *instance = [CameraManager sharedInstance];
        instance.cameraList = nil;
        instance.cameraList = newCameraList;
    }
}

+ (NSMutableArray*)getCameraList {
    CameraManager *instance = [CameraManager sharedInstance];
    return instance.cameraList;
}

+ (void)restoreCameraListFromDefaults {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray *defaultCameraList = [defaults objectForKey:CAMERA_LIST];
    if (defaultCameraList) {
        CameraManager *instance = [CameraManager sharedInstance];
        instance.cameraList = nil;
        instance.cameraList = defaultCameraList;
    }
}

+ (void)syncCameraListToDefaults {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray *cameraList = [CameraManager getCameraList];
    if (cameraList) {
        [defaults setObject:cameraList forKey:CAMERA_LIST];
    }
}

+ (void)resetCameraValidation {
    NSMutableArray *cameraListCopy = [[CameraManager getCameraList] mutableCopy];
    for (NSMutableDictionary *cameraDict in cameraListCopy) {
        NSString *uid = [cameraDict objectForKey:CAMERA_UID];
        if (uid && ![uid isEqualToString:@""]) {
            [CameraManager updateCamera:uid withObject:@0 andKey:SAT_VALID];
            [CameraManager updateCamera:uid withObject:@0 andKey:WAN_VALID];
            [CameraManager updateCamera:uid withObject:@0 andKey:TALK_VALID];
        }
    }
}

// Return index of a camera if UID is found in camera list
+ (NSInteger)findCameraIndexWithUID:(NSString*)UID {
    NSInteger index = [CAMERA_NOT_FOUND integerValue];

    NSMutableArray *cameraList = [CameraManager getCameraList];
    if (cameraList && UID) {
        for (NSMutableDictionary *cameraDict in cameraList) {
            if ([UID isEqualToString:[cameraDict objectForKey:CAMERA_UID]]) {
                index = [cameraList indexOfObject:cameraDict];
                break;
            }
        }
    }
    
    return index;
}

// Return index of a camera if MAC is found in camera list
+ (NSInteger)findCameraIndexWithMAC:(NSString*)MAC {
    NSInteger index = [CAMERA_NOT_FOUND integerValue];
    
    NSMutableArray *cameraList = [CameraManager getCameraList];
    if (cameraList && MAC) {
        for (NSMutableDictionary *cameraDict in cameraList) {
            if ([MAC isEqualToString:[cameraDict objectForKey:CAMERA_MAC]]) {
                index = [cameraList indexOfObject:cameraDict];
                break;
            }
        }
    }
    
    return index;
}

+ (void)updateCamera:(NSString*)UID withObject:(id)object andKey:(NSString*)key {
    // Check existence first
    NSInteger index = [CameraManager findCameraIndexWithUID:UID];
    NSMutableArray *cameraListCopy = [[CameraManager getCameraList] mutableCopy];
    
    // Found existence
    if (index != [CAMERA_NOT_FOUND integerValue]) {
        NSMutableDictionary *cameraDictCopy = [[cameraListCopy objectAtIndex:index] mutableCopy];
        [cameraDictCopy setObject:object forKey:key];
        [cameraListCopy replaceObjectAtIndex:index withObject:cameraDictCopy];
        [CameraManager setCamCameraList:cameraListCopy];
    }
    // New entry
    else {
        NSMutableDictionary *newCameraDict = [NSMutableDictionary dictionary];
        [newCameraDict setObject:UID forKey:CAMERA_UID];
        [newCameraDict setObject:object forKey:key];
        [CameraManager batchUpdateCamera:newCameraDict];
    }
}

+ (void)updateCameraMAC:(NSString*)MAC withObject:(id)object andKey:(NSString*)key {
    // Check existence first
    NSInteger index = [CameraManager findCameraIndexWithMAC:MAC];
    NSMutableArray *cameraListCopy = [[CameraManager getCameraList] mutableCopy];
    
    // Found existence
    if (index != [CAMERA_NOT_FOUND integerValue]) {
        NSMutableDictionary *cameraDictCopy = [[cameraListCopy objectAtIndex:index] mutableCopy];
        [cameraDictCopy setObject:object forKey:key];
        [cameraListCopy replaceObjectAtIndex:index withObject:cameraDictCopy];
        [CameraManager setCamCameraList:cameraListCopy];
    }
    // New entry
    else {
        NSMutableDictionary *newCameraDict = [NSMutableDictionary dictionary];
        [newCameraDict setObject:MAC forKey:CAMERA_MAC];
        [newCameraDict setObject:object forKey:key];
        [CameraManager batchUpdateCamera:newCameraDict];
    }
}

+ (void)batchUpdateCamera:(NSMutableDictionary*)cameraInfo {
    // Check existence first
    NSString *UID = [cameraInfo objectForKey:CAMERA_UID];
    NSString *MAC = [cameraInfo objectForKey:CAMERA_MAC];
    if (!UID && !MAC) {
        return;
    }
    
    NSInteger index = [CAMERA_NOT_FOUND integerValue];
    if (UID) {
        index = [CameraManager findCameraIndexWithUID:UID];
//        NSLog(@"Brosso: found UID: %d", index);
    }
    else if (MAC) {
        index = [CameraManager findCameraIndexWithMAC:MAC];
//        NSLog(@"Brosso: found MAC: %d", index);
    }
    NSMutableArray *cameraListCopy = [[CameraManager getCameraList] mutableCopy];
    
    // Found existence
    if (index != [CAMERA_NOT_FOUND integerValue]) {
//        NSLog(@"Brosso: update old entry");
        NSDictionary *cameraDict = [cameraListCopy objectAtIndex:index];
        [cameraInfo addEntriesFromDictionary:cameraDict];
        [cameraListCopy replaceObjectAtIndex:index withObject:cameraInfo];
    }
    // New entry
    else {
//        NSLog(@"Brosso: add new entry");
        [cameraListCopy addObject:cameraInfo];
    }
    
    [CameraManager setCamCameraList:cameraListCopy];
}

#pragma mark LAN Search
+ (void)broadcastLAN {
    NSLog(@"Brosso: AONI start search");
    SearchIPCamWrapper *AONI_Wrapper = [SearchIPCamWrapper sharedInstance];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        NSLog(@"Brosso: AONI search after 5 secs");
        
        NSArray *aoniResults = [[NSSet setWithArray:[AONI_Wrapper getAllInfo]] allObjects];
        
        for (NSDictionary *dict in aoniResults) {
            NSString *strMac = [[[dict objectForKey:KEY_MAC] uppercaseString] stringByReplacingOccurrencesOfString:@":" withString:@""];
            NSString *strIP = [dict objectForKey:KEY_IP];
            NSLog(@"Brosso: AONI device found!!! MAC:%@, IP:%@",strMac,strIP);
            [CameraManager updateCameraMAC:strMac withObject:strIP andKey:LAN_IP];
            [CameraManager updateCameraMAC:strMac withObject:@1 andKey:LAN_VALID];
        }
    });
    
}

@end
