//
//  SATWrapper.m
//  GynoiiBaby
//
//  Created by BingHuan Wu on 3/9/17.
//  Copyright © 2017 Gynoii. All rights reserved.
//

#import "SATWrapper.h"

@implementation SATWrapper

+ (id)sharedInstance {
    // structure used to test whether the block has completed or not
    static dispatch_once_t p = 0;
    
    // initialize sharedObject as nil (first call only)
    __strong static id _sharedObject = nil;
    
    // executes a block object once and only once for the lifetime of an application
    dispatch_once(&p, ^{
        _sharedObject = [[self alloc] init];
        if (_sharedObject) {
            ((SATWrapper*) _sharedObject).manager = [[SatManager alloc] init];
            [((SATWrapper*) _sharedObject).manager setDebugLevel:10];
        }
    });
    
    // returns the same object each time
    return _sharedObject;
}

+ (SatManager*)sharedManager {
    SATWrapper *instance = [SATWrapper sharedInstance];
    return instance.manager;
}

// Important: Has NOTHING to do with username/password correctness
// YES -> QLYNC service is alive
// NO -> QLYNC service is dead
+ (BOOL)loginwithUsername:(NSString*)username andPassword:(NSString*)password {
    SatManager *manager = [SATWrapper sharedManager];
    if (manager) {
        return [manager initSat:username password:password];
    }
    
    return NO;
}
// Important: Always call this after login
// non nil -> login success with array,count cameras
// nil -> login fail
+ (NSMutableArray *)getCameraList {
    SatManager *manager = [SATWrapper sharedManager];
    
    if (manager) {
        NSMutableArray *cameraList = [manager gynoii_getDeviceEntryList];
        return cameraList;
    }
    return nil;
}

+ (NSDictionary *)startP2PWithUID:(NSString*)uid andPort:(NSUInteger)port {
    SatManager *manager = [SATWrapper sharedManager];
    
    if (manager) {
        NSDictionary *tunnelInfo = [manager startCaller:uid port:port];
        if (tunnelInfo) {
            int portValue = [[tunnelInfo objectForKey:@"port"] intValue];
            NSNumber *portNumber = [NSNumber numberWithInt:portValue];
            
            switch (port) {
                case RTSP_PORT_NUMBER: { // RTSP
                    NSString *satPath = [NSString stringWithFormat:@"rtsp://127.0.0.1:%d", portValue];
                    [CameraManager updateCamera:uid withObject:satPath andKey:SAT_PATH];
                    [CameraManager updateCamera:uid withObject:@1 andKey:SAT_VALID];
                    [CameraManager updateCamera:uid withObject:portNumber andKey:RTSP_PORT];
                    break;
                }
                case WAN_PORT_NUMBER: { // CGI WAN
                    [CameraManager updateCamera:uid withObject:@1 andKey:WAN_VALID];
                    [CameraManager updateCamera:uid withObject:portNumber andKey:WAN_PORT];
                    break;
                }
                case TALK_PORT_NUMBER: { // TWO-WAY
                    [CameraManager updateCamera:uid withObject:@1 andKey:TALK_VALID];
                    [CameraManager updateCamera:uid withObject:portNumber andKey:TALK_PORT];
                    break;
                }
            }
        }
        
        // Sync back to user defaults
        [CameraManager syncCameraListToDefaults];
        return tunnelInfo;
    }
    return nil;
}

+ (NSMutableArray *)peepMode:(NSMutableArray *)input {
    NSMutableArray *output = [NSMutableArray arrayWithArray:input];
    
    // This is only for customer service. Make sure to comment out before release
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setValue:@"A07CC-E8ABFA1B4354" forKey:CAMERA_UID]; //A07CC-E8ABFA1B508D
    [dict setValue:[NSNumber numberWithUnsignedInteger:554] forKey:SAT_PORT];
    [dict setValue:@"/live/ch01_0" forKey:STREAM_PATH];
    [dict setValue:@"/live/ch01_0" forKey:STREAM_PATH_LOW];
    [dict setValue:@"/live/ch01_0" forKey:STREAM_PATH_MID];
    [dict setValue:@"/live/ch00_0" forKey:STREAM_PATH_HIGH];
    [dict setValue:@"E8ABFA1B508D" forKey:CAMERA_MAC];
    [dict setValue:@"PEEP_AONI" forKey:CAMERA_NAME];
    [dict setValue:@1 forKey:CAMERA_STATUS];
    [dict setValue:@0 forKey:SAT_VALID];
    [dict setValue:@0 forKey:LAN_VALID];
    [dict setValue:@"/custom/snapshot.cgi" forKey:SNAPSHOT_PATH];
    [output addObject:dict];
    
    NSMutableDictionary *dict2 = [NSMutableDictionary dictionary];
    [dict2 setValue:@"J02CC-002509692356" forKey:CAMERA_UID];
    [dict2 setValue:[NSNumber numberWithUnsignedInteger:554] forKey:SAT_PORT];
    [dict2 setValue:@"/live1.264" forKey:STREAM_PATH];
    [dict2 setValue:@"/live1.264" forKey:STREAM_PATH_LOW];
    [dict2 setValue:@"/live1.264" forKey:STREAM_PATH_MID];
    [dict2 setValue:@"/live0.264" forKey:STREAM_PATH_HIGH];
    [dict2 setValue:@"002509692356" forKey:CAMERA_MAC];
    [dict2 setValue:@"PEEP_JOVISION" forKey:CAMERA_NAME];
    [dict2 setValue:@0 forKey:CAMERA_STATUS];
    [dict2 setValue:@0 forKey:SAT_VALID];
    [dict2 setValue:@0 forKey:LAN_VALID];
    [dict2 setValue:@"192.168.1.104" forKey:@"LAN_IP"];
    [dict2 setValue:@"/cgi-bin/getsnapshot.cgi" forKey:SNAPSHOT_PATH];
    [output addObject:dict2];
    
    return output;
}

+ (void)connectAllCameras { // All cam, all port
    NSMutableArray *cameraList = [[CameraManager getCameraList] mutableCopy];
    
    // Debug only - fake camera list
//    cameraList = [SATWrapper peepMode:cameraList];
//    [CameraManager setCamCameraList:cameraList];
    
    
    NSMutableArray *jobQueue = [NSMutableArray array];
    
    if (cameraList && [cameraList count]) {
        for (NSMutableDictionary *cameraDict in cameraList) {
            NSString *uid = [cameraDict objectForKey:CAMERA_UID];
            if (uid) {
                [jobQueue addObject:uid];
            }
        }
    }
    
    [SATWrapper executeJobQueue:jobQueue withPort:0];
}

+ (void)connectAllCamerasOnPort:(int)port { // All cam, one port
    NSMutableArray *cameraList = [[CameraManager getCameraList] mutableCopy];
    cameraList = [SATWrapper peepMode:cameraList];
    NSMutableArray *jobQueue = [NSMutableArray array];
    
    if (cameraList && [cameraList count]) {
        for (NSMutableDictionary *cameraDict in cameraList) {
            NSString *uid = [cameraDict objectForKey:CAMERA_UID];
            if (uid) {
                [jobQueue addObject:uid];
            }
        }
    }
    
    [SATWrapper executeJobQueue:jobQueue withPort:port];
}

+ (void)connectCamera:(NSString*)uid { // One cam, all port
    [SATWrapper executeJobQueue:[NSMutableArray arrayWithObjects:uid, nil] withPort:0];
}

+ (void)connectCamera:(NSString*)uid onPort:(int)port { // One cam, one port
    [SATWrapper executeJobQueue:[NSMutableArray arrayWithObjects:uid, nil] withPort:port];
}

+ (void)executeJobQueue:(NSMutableArray *)jobQueue withPort:(int)port {
    
    // Create serial queue
    dispatch_queue_t serialQueue = dispatch_queue_create("com.gynoii.serial.queue", DISPATCH_QUEUE_SERIAL);
    
    if (port) { // Specify port
        for (NSString *uid in jobQueue) {
            dispatch_async(serialQueue, ^{  // dispatch_get_global_queue(0, 0)
                [SATWrapper startP2PWithUID:uid andPort:port];
            });
        }
    }
    else { // Connect all ports
        for (NSString *uid in jobQueue) {
            dispatch_async(serialQueue, ^{  // dispatch_get_global_queue(0, 0)
                [SATWrapper startP2PWithUID:uid andPort:RTSP_PORT_NUMBER];
            });
        }
        for (NSString *uid in jobQueue) {
            dispatch_async(serialQueue, ^{  // dispatch_get_global_queue(0, 0)
                [SATWrapper startP2PWithUID:uid andPort:WAN_PORT_NUMBER];
            });
        }
        for (NSString *uid in jobQueue) {
            dispatch_async(serialQueue, ^{  // dispatch_get_global_queue(0, 0)
                [SATWrapper startP2PWithUID:uid andPort:TALK_PORT_NUMBER];
            });
        }
    }
}

@end
