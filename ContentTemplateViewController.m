//
//  ContentTemplateViewController.m
//  GynoiiBaby
//
//  Created by BingHuan Wu on 4/7/17.
//  Copyright © 2017 Gynoii. All rights reserved.
//

#import "ContentTemplateViewController.h"

@interface ContentTemplateViewController () {
    BOOL mode;
}

@end

@implementation ContentTemplateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated {
    if (mode) {
        [self startAnimation];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setMode:(BOOL)flag {
    mode = flag;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if (flag) {
            [self startAnimation];
        }
        else {
            [self stopAnimation];
        }
        
        loading.hidden = !flag;
        cloud.hidden = flag;
        
        // Localization
        [statusLabel setText:flag? NSLocalizedString(@"Loading...", nil): NSLocalizedString(@"YOUR CAMERA IS OFFLINE", nil)];
    });
}

- (void)startAnimation {
    // Forever rotation
    CABasicAnimation* animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    animation.fromValue = [NSNumber numberWithFloat:0.0f];
    animation.toValue = [NSNumber numberWithFloat: 2*M_PI];
    animation.duration = 3.0f;
    animation.repeatCount = HUGE_VAL;
    [loading.layer addAnimation:animation forKey:@"ForeverRotation"];
}

- (void)stopAnimation {
    [loading.layer removeAllAnimations];
}

- (UIView * _Nonnull)getRenderView {
    return renderView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
