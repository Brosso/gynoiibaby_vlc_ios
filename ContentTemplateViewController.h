//
//  ContentTemplateViewController.h
//  GynoiiBaby
//
//  Created by BingHuan Wu on 4/7/17.
//  Copyright © 2017 Gynoii. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContentTemplateViewController : UIViewController {
    IBOutlet UILabel *statusLabel;
    IBOutlet UIImageView *loading;
    IBOutlet UIImageView *cloud;
    IBOutlet UIView *renderView;
}

- (void)setMode:(BOOL)flag;
- (void)startAnimation;
- (void)stopAnimation;
- (UIView * _Nonnull)getRenderView;

@end
